package com.xonware.xims.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.error.ANError
import com.xonware.xims.R
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.gatewaylist.Glst


class OnlineGatewayAdapter(
    private val context: Context,
    private val addTicketResponne: List<Glst>
) : RecyclerView.Adapter<OnlineGatewayAdapter.MyViewHolder>(), WebServices.SetResponse {

    private var lastSelectedPosition = -1
    private var mOnClickListener: MyOnClickListener? = null

    override fun onSuccess(methodName: String, response: String?) {
        println("reposbne for invoice is " + response)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var btnInstamojo: TextView = view.findViewById(R.id.btnInstamojo)
        var gatewayLogo: ImageView = view.findViewById(R.id.gatewayLogo)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_gateway, parent, false)

        return MyViewHolder(itemView)
    }

    fun setOnItemClickListener(clickListener: MyOnClickListener) {
        mOnClickListener = clickListener
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = addTicketResponne!![position]
        holder.btnInstamojo.text = "Pay Using ${movie.gatewayName!!.replace("APP","")}"

        holder.btnInstamojo.setOnClickListener {
            mOnClickListener!!.onRecyclerItemClick(movie)
        }
        when  {
            movie.gatewayName!!.contains("instamojo",true)-> {
                holder.gatewayLogo.setImageDrawable(context.resources.getDrawable(R.drawable.instamojo_logo))
            }
            movie.gatewayName!!.contains("paytm",true) -> {
                holder.gatewayLogo.setImageDrawable(context.resources.getDrawable(R.drawable.paytm_logo))
            }
        }


/*
        if (lastSelectedPosition==position){
            holder.cbTitle.isChecked = true
            holder.cardTicket.setBackgroundColor(context.resources.getColor(R.color.screenBg))
        }else{

        }*/


        /*holder.cbTitle.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                addTicketResponne.selectedOption = movie!!.id!!
                println("selected item is "+addTicketResponne.selectedOption)
            }
        }*/
    }

    override fun getItemCount(): Int {
        return addTicketResponne!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    interface MyOnClickListener {
        fun onRecyclerItemClick(movie: Glst)
    }
}