package com.xonware.xims.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xonware.xims.R
import com.xonware.xims.response.datausages.DataList
import kotlin.collections.ArrayList

class DataUsageAdapter(
    private val context: Context,
    private val arrayList: ArrayList<DataList>
) : RecyclerView.Adapter<DataUsageAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvStartDate: TextView
        var tvEndDate: TextView
        var tvSessionTime: TextView
        var tvMacId: TextView
        var tvIpAddress: TextView
        var tvUpload: TextView
        var tvDownload: TextView
        var tvTotalData: TextView

        init {
            tvStartDate = view.findViewById(R.id.tvStartDate)
            tvEndDate = view.findViewById(R.id.tvEndDate)
            tvSessionTime = view.findViewById(R.id.tvSessionTime)
            tvMacId = view.findViewById(R.id.tvMacId)
            tvIpAddress = view.findViewById(R.id.tvIpAddress)
            tvUpload = view.findViewById(R.id.tvUpload)
            tvDownload = view.findViewById(R.id.tvDownload)
            tvTotalData = view.findViewById(R.id.tvTotalData)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_data_usage, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = arrayList[position]
        holder.tvStartDate.setText(": "+movie.sessionStartDateS)
        holder.tvEndDate.setText(": "+movie.sessionEndDateS)
        holder.tvMacId.setText(": "+movie.callerId)
        holder.tvSessionTime.setText(": "+movie.sessionTime)
        holder.tvIpAddress.setText(": "+movie.ipAddress)
        holder.tvUpload.setText(movie.bytesOut)
        holder.tvUpload.setTextColor(ContextCompat.getColor(context,R.color.color_upload))
        holder.tvDownload.setText(movie.bytesIn)
        holder.tvDownload.setTextColor(ContextCompat.getColor(context,R.color.color_download))
        holder.tvTotalData.setText(movie.byteTotal)
        holder.tvTotalData.setTextColor(ContextCompat.getColor(context,R.color.ccolor_total))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}