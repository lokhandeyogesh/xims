package com.xonware.xims.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.response.invoices.InvList
import com.xonware.xims.response.tickets.STList
import java.util.ArrayList


class TicketsAdapter(
    private val context: Context,
    private val arrayList: ArrayList<STList>
) : RecyclerView.Adapter<TicketsAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvDate: TextView
        var tvTicketNumber: TextView
        var tvTicketTitle: TextView
        var tvTicketDescription: TextView
        var tvComment: TextView
        var tvTicketStatus: TextView
        var tvCommentTl: TextView

        init {
            tvDate = view.findViewById(R.id.tvDateTickets)
            tvTicketNumber = view.findViewById(R.id.tvTicketNumber)
            tvTicketTitle = view.findViewById(R.id.tvTicketTitle)
            tvComment = view.findViewById(R.id.tvComment)
            tvTicketDescription = view.findViewById(R.id.tvTicketDescription)
            tvTicketStatus = view.findViewById(R.id.tvTicketStatus)
            tvCommentTl = view.findViewById(R.id.tvCommentTl)
//            rowComment = view.findViewById(R.id.rowComment)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_tickets_new, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = arrayList[position]
        var test = movie.ticketDate!!
        test = test.substring(0,test.indexOf("T"))
        println(test)
        holder.tvDate.text = test
        holder.tvTicketNumber.text = movie.ticketNo

        if (movie.comment != null) {
            holder.tvComment.text = "${movie.comment}"
            holder.tvCommentTl.visibility = View.VISIBLE
            holder.tvComment.visibility = View.VISIBLE
        } else {
            holder.tvComment.visibility = View.GONE
            holder.tvCommentTl.visibility = View.GONE
        }
        holder.tvTicketDescription.text = "${movie.ticketDesc}"
        holder.tvTicketTitle.text = movie.sTicketOption
        holder.tvTicketStatus.text = movie.supportStatus

        val bgShape = holder.tvTicketStatus.background as GradientDrawable
        when {
            movie.supportStatus!!.contains("progress", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.yellow))
                //  holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.yellow))
            }
            movie.supportStatus!!.contains("not", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.red))
                // holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.red))
            }
            movie.supportStatus!!.contains("resolved", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.green))
                // holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.green))
            }
            movie.supportStatus!!.contains("hold", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.orange))
                // holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.orange))
            }
            movie.supportStatus!!.contains("open", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.grey))
                // holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.grey))
            }
            movie.supportStatus!!.contains("close", true) -> {
                bgShape.setColor(ContextCompat.getColor(context, R.color.black))
                //holder.tvTicketStatus.setTextColor(ContextCompat.getColor(context,R.color.black))
            }
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}