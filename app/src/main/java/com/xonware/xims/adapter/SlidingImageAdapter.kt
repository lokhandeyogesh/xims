package com.xonware.xims.adapter

import android.content.Context
import android.os.Parcelable
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.xonware.xims.R


class SlidingImageAdapter(private val context: Context, private val IMAGES: ArrayList<String>) : PagerAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun destroyItem(container: ViewGroup, position: Int, objec: Any) {
        container.removeView(objec as View)
    }

    override fun getCount(): Int {
        return IMAGES.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimage_layout, view, false)!!

        val imageView = imageLayout!!
            .findViewById(R.id.image) as ImageView


        //imageView.setImageResource(IMAGES[position])
        println("image path is "+context.getString(R.string.api_base_url).replace("api/","")+IMAGES[position])
        Glide.with(context)
            .load(context.getString(R.string.api_base_url).replace("api/","")+IMAGES[position])
            .into(imageView)

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, objec: Any): Boolean {
        return view.equals(objec)
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }
}