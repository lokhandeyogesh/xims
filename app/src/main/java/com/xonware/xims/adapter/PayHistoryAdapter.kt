package com.xonware.xims.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.xonware.xims.response.invoices.InvList
import java.util.ArrayList
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import android.text.Html
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.error.ANError
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import kotlinx.android.synthetic.main.activity_home.*


class PayHistoryAdapter(
    private val context: Context,
    private val arrayList: ArrayList<InvList>
) : RecyclerView.Adapter<PayHistoryAdapter.MyViewHolder>(), WebServices.SetResponse {
    override fun onSuccess(methodName: String, response: String?) {
        println("reposbne for invoice is " + response)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvDate: TextView
        var tvInvoiceNumber: TextView
        //var tvTransactId: TextView
        //  var tvPlanValidity: TextView
        //var tvPaymentStatus: TextView
        var tvPayStatus: TextView
        var tvPlan: TextView
        var tvPaymentMode: TextView
        var tvPaymentAmount: TextView

        init {
            tvDate = view.findViewById(R.id.tvDate)
            tvInvoiceNumber = view.findViewById(R.id.tvInvoiceNumber)
//            tvTransactId = view.findViewById(R.id.tvTransactId)
            //   tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus)
//            tvPlanValidity = view.findViewById(R.id.tvPlanValidity)
            tvPlan = view.findViewById(R.id.tvPlan)
            tvPaymentMode = view.findViewById(R.id.tvPaymentMode)
            tvPaymentAmount = view.findViewById(R.id.tvPaymentAmount)
            tvPayStatus = view.findViewById(R.id.tvPayStatus)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pay_history, parent, false)

        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = arrayList[position]
        println("pay history adapter "+movie.planValidity)
        holder.tvDate.text =
            Html.fromHtml(/*"<b>Date: </b>"+*/((context as BaseActivity).getDateFromString(movie.transactionDate!!)))
        holder.tvInvoiceNumber.text = Html.fromHtml(/*"<b>Invoice Number: </b>" +*/ movie.invoiceNo)
        /*holder.tvTransactId.setText("Transaction Id is ${movie.transactionId}")
        holder.tvPlanValidity.setText("Plan is ${movie.planName} with \n${movie.planValidity} validity")
        holder.tvPaymentStatus.setText("Payment made of \u20B9 ${movie.amount} by ${movie.paymentMode} \n status is ${movie.paymentStatus}")*/

        // holder.tvTransactId.text = Html.fromHtml("<b>Trans. Id: </b>" + movie.transactionId)
        //  holder.tvPlanValidity.text = ": " + movie.planValidity
        holder.tvPlan.text = movie.planName + " & " + movie.planValidity
        val preferences = Preferences.getInstance(context)
        if (!preferences!!.getPreferencesBoolean(context.getString(R.string.pref_display_plan), false)&& !preferences!!.getPreferencesString(context.getString(R.string.pref_cust_type))!!.contains("sharing")) {
            holder.tvPaymentAmount.visibility = View.GONE
        }
        holder.tvPaymentAmount.text = "₹ ${movie.amount}"
        holder.tvPaymentMode.text = movie.paymentMode
        // holder.tvPaymentStatus.text = ": " + movie.paymentStatus

        //holder.tvPayStatus.visibility = View.INVISIBLE
        // holder.ivView.visibility = View.GONE
        val bgShape = holder.tvPayStatus.background as GradientDrawable

        if (!movie.paymentStatus.equals("Success",true)) {
            bgShape.setColor(ContextCompat.getColor(context, R.color.red))
            holder.tvPayStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(context, R.drawable.ic_cross),
                null,
                null
            )
            holder.tvPayStatus.text = movie.paymentStatus
        } else {
            bgShape.setColor(ContextCompat.getColor(context, R.color.green))
            holder.tvPayStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(context, R.drawable.ic_checked),
                null,
                null
            )
            holder.tvPayStatus.text = movie.paymentStatus
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}