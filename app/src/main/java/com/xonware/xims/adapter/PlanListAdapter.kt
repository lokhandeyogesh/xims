package com.xonware.xims.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xonware.xims.R
import com.xonware.xims.response.plan_list.PDlst
import com.xonware.xims.response.plan_list.Plan
import java.util.*
import kotlin.collections.ArrayList


class PlanListAdapter(
    private val context: Context,
    private val arrayList: ArrayList<Plan>
) : RecyclerView.Adapter<PlanListAdapter.MyViewHolder>() {

    private var currentPosition = -98

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvPlanTitle: TextView = view.findViewById(R.id.tvPlanTitle)
        var rvPlanDetails: RecyclerView = view.findViewById(R.id.rvPlanDetails)
        var llValidity: LinearLayout = view.findViewById(R.id.llValidity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_expandable_recyclerview, parent, false)

        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = arrayList[position]
        holder.tvPlanTitle.text = movie.planName + " / " + movie.planCategory
        holder.rvPlanDetails.visibility = View.GONE
        holder.llValidity.visibility = View.GONE
        var arrPdList = ArrayList<PDlst>()
        for (i in 0 until movie.pDlst!!.size) {
            if (movie!!.pDlst!![i].amount != null && movie!!.pDlst!![i].amount != 0f) {
                arrPdList.add(movie!!.pDlst!![i])
            }
        }
        val mAdapter = PlanListCategoryAdapter(context!!, arrPdList)
        val mLayoutManager = LinearLayoutManager(context!!)
        holder.rvPlanDetails.layoutManager = mLayoutManager
        val dividerItemDecoration = DividerItemDecoration(
            holder.rvPlanDetails.context,
            mLayoutManager.orientation
        )
        holder.rvPlanDetails.addItemDecoration(dividerItemDecoration)
        holder.rvPlanDetails.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        holder.rvPlanDetails.adapter = mAdapter


        if (currentPosition == position) {
            holder.llValidity.visibility = View.VISIBLE
            holder.rvPlanDetails.visibility = View.VISIBLE
            holder.tvPlanTitle.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_expand_arrow,
                0
            );
        }

        holder.tvPlanTitle.setOnClickListener {
            if (position != currentPosition) {
                currentPosition = position
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        println("size id ${arrayList.size}")
        return arrayList.size
    }
}

class PlanListCategoryAdapter(
    private val context: Context,
    private val arrayList: List<PDlst>?
) : RecyclerView.Adapter<PlanListCategoryAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvplanValidity: TextView = view.findViewById(R.id.tvplanValidity)
        var tvPlanPrice: TextView = view.findViewById(R.id.tvPlanPrice)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_plan_validity, parent, false)

        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = arrayList!![position]
        holder.tvplanValidity.text = movie.billingcycle
        holder.tvPlanPrice.text = movie.amount.toString()
    }

    override fun getItemCount(): Int {
        println("size id ${arrayList!!.size}")
        return arrayList!!.size
    }
}