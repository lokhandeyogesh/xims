package com.xonware.xims.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.error.ANError
import com.xonware.xims.R
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.add_ticket.AddTicketResponse


class CreateTicketAdapter(
    private val context: Context,
    private val addTicketResponne: AddTicketResponse?
) : RecyclerView.Adapter<CreateTicketAdapter.MyViewHolder>(), WebServices.SetResponse {

    private var lastSelectedPosition = -1

    override fun onSuccess(methodName: String, response: String?) {
        println("reposbne for invoice is " + response)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cbTitle: RadioButton
        var cardTicket: CardView

        init {
            cbTitle = view.findViewById(R.id.cbTitle)
            cardTicket = view.findViewById(R.id.cardTicket)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_create_ticket, parent, false)

        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = addTicketResponne!!.oList!![position]
        holder.cbTitle.text = movie.optionS
        holder.cbTitle.isChecked = lastSelectedPosition == position
/*
        if (lastSelectedPosition==position){
            holder.cbTitle.isChecked = true
            holder.cardTicket.setBackgroundColor(context.resources.getColor(R.color.screenBg))
        }else{

        }*/

        holder.cbTitle.setOnClickListener {
            lastSelectedPosition = holder.adapterPosition
            notifyDataSetChanged()
            addTicketResponne.selectedOption = movie!!.id!!
            println("selected item is "+addTicketResponne.selectedOption)
          /*  Toast.makeText(
                context,
                "selected offer is " + movie.optionS,
                Toast.LENGTH_LONG
            ).show()*/
        }

        /*holder.cbTitle.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                addTicketResponne.selectedOption = movie!!.id!!
                println("selected item is "+addTicketResponne.selectedOption)
            }
        }*/
    }

    override fun getItemCount(): Int {
        return addTicketResponne!!.oList!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}