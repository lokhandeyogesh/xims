package com.xonware.xims.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson

import com.xonware.xims.R
import com.xonware.xims.activities.CreateTicketActivity
import com.xonware.xims.adapter.TicketsAdapter
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.tickets.STList
import com.xonware.xims.response.tickets.TicketResponse
import kotlinx.android.synthetic.main.fragment_tickets.*
import java.util.ArrayList

class TicketsFragment : Fragment(), WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tickets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)

        println("tickets called ")
        fab.setOnClickListener {
            startActivity(Intent(activity!!,CreateTicketActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        getTicketList()
    }

    private fun getTicketList() {
        webServices!!.callWebPostService(
            getString(R.string.api_tickets) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)),
            mutableMapOf()
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        if (isAdded) {
            val invoicesResponse = Gson().fromJson(response, TicketResponse::class.java)
            println("response tickets $response")
            if (invoicesResponse.stList != null) {
                val mAdapter = TicketsAdapter(activity!!, invoicesResponse.stList as ArrayList<STList>)
                val mLayoutManager = LinearLayoutManager(activity!!)
                val dividerItemDecoration = DividerItemDecoration(
                    rvTickets.context,
                    mLayoutManager.orientation
                )
                rvTickets.addItemDecoration(dividerItemDecoration)
                rvTickets.layoutManager = mLayoutManager
                rvTickets.itemAnimator = DefaultItemAnimator()
                rvTickets.adapter = mAdapter
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}