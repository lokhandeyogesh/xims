package com.xonware.xims.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import kotlinx.android.synthetic.main.fragment_invoices.*
import java.util.ArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.xonware.xims.adapter.PlanListAdapter
import com.xonware.xims.response.plan_list.Plan
import com.xonware.xims.response.plan_list.PlanListResponse


@SuppressLint("ValidFragment")
class PlansListFragment() : Fragment(), WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invoices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // flLayout.setBackgroundColor(Color.pa)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)

        webServices!!.callWebPostService(
            getString(R.string.api_plan_list) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)),
            mutableMapOf()
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        if (isAdded) {
            val invoicesResponse = Gson().fromJson(response, PlanListResponse::class.java)
            println("response invoices $response")
            // if (invoicesResponse.res != null) {
            var listArray = ArrayList<Plan>()

            for (i in 0 until invoicesResponse.planNew!!.size) {
                for (j in 0 until invoicesResponse!!.planNew!![i].plan!!.size) {
                    val datum = Plan()
                    datum.id = invoicesResponse!!.planNew!![i].plan!![j].id
                    datum.days = invoicesResponse!!.planNew!![i].plan!![j].days
                    datum.pDlst = invoicesResponse!!.planNew!![i].plan!![j].pDlst
                    datum.planCategory = invoicesResponse!!.planNew!![i].plan!![j].planCategory
                    datum.planDataspeed = invoicesResponse!!.planNew!![i].plan!![j].planDataspeed
                    datum.planName = invoicesResponse!!.planNew!![i].plan!![j].planName
                    datum.planType = invoicesResponse!!.planNew!![i].plan!![j].planType
                    listArray.add(datum)
                }
            }

            val mAdapter = PlanListAdapter(activity!!, listArray)
            val mLayoutManager = LinearLayoutManager(activity!!)
            rvInvoices.layoutManager = mLayoutManager
            val dividerItemDecoration = DividerItemDecoration(
                rvInvoices.context,
                mLayoutManager.orientation
            )
            rvInvoices.addItemDecoration(dividerItemDecoration)
            rvInvoices.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
            rvInvoices.adapter = mAdapter
        }
        // }
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
