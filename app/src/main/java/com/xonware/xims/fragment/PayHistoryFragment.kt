package com.xonware.xims.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.invoices.InvList
import com.xonware.xims.response.invoices.InvoicesResponse
import kotlinx.android.synthetic.main.fragment_invoices.*
import java.util.ArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import com.xonware.xims.adapter.PayHistoryAdapter


@SuppressLint("ValidFragment")
class PayHistoryFragment() : Fragment(), WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invoices, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)

        webServices!!.callWebPostService(
            getString(R.string.api_invoices) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)),
            mutableMapOf()
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        if(isAdded) {
            val invoicesResponse = Gson().fromJson(response, InvoicesResponse::class.java)
            println("response invoices $response")
            if (invoicesResponse.invList != null) {
                val mAdapter = PayHistoryAdapter(activity!!, invoicesResponse.invList as ArrayList<InvList>)
                val mLayoutManager = LinearLayoutManager(activity!!)
                rvInvoices.layoutManager = mLayoutManager
                val dividerItemDecoration = DividerItemDecoration(
                    rvInvoices.context,
                    mLayoutManager.orientation
                )
                rvInvoices.addItemDecoration(dividerItemDecoration)
                rvInvoices.itemAnimator = DefaultItemAnimator()
                rvInvoices.adapter = mAdapter
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
