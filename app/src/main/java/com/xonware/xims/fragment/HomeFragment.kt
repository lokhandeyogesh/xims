package com.xonware.xims.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.activities.DataUsageActivity
import com.xonware.xims.activities.EditProfileActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import android.util.Log
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.xonware.xims.activities.RenewPlanActivity
import com.xonware.xims.adapter.SlidingImageAdapter
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.response.dashboard.DashboardResponse
import kotlinx.android.synthetic.main.fragment_home.any_chart_view
import kotlinx.android.synthetic.main.fragment_home.btnAdvRenew
import kotlinx.android.synthetic.main.fragment_home.btnInstantRenew
import kotlinx.android.synthetic.main.fragment_home.btnViewUsage
import kotlinx.android.synthetic.main.fragment_home.tvPlanStatus
import kotlinx.android.synthetic.main.fragment_home.tvPlanValidity
import kotlinx.android.synthetic.main.fragment_home_new.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment(), View.OnClickListener, WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null
    private var currentPage = 0
    private var NUM_PAGES = 0
    var handler = Handler()
    var swipeTimer = Timer()
    private var update: Runnable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)


        /*      If(UserOnlinePayment == true && CustType == "Sharing" && IsCrossMaxUnpaidInvoiceLimit == false && IsPaymentGateway == true)
              {
      // Show button for instant renew
              }*/

//        setRenewButtonView()


//        ivEditProfile.setOnClickListener(this)
        tvPlanStatus.setOnClickListener(this)
        btnViewUsage.setOnClickListener(this)
        btnInstantRenew.setOnClickListener(this)
        btnAdvRenew.setOnClickListener(this)
        getUserDashboard()
        /// setDashboardImages()
    }

    private fun setRenewButtonView(dashboardResponse: DashboardResponse) {
        if (!preferences!!.getPreferencesString(getString(R.string.pref_IsCrossMaxUnpaidInvoiceLimit))
                .isNullOrEmpty()
        ) {
            if (preferences!!.getPreferencesString(getString(R.string.pref_IsCrossMaxUnpaidInvoiceLimit)) == "false"
                && preferences!!.getPreferencesString(getString(R.string.pref_cust_type))!!
                    .contains("sharing", true)
                && preferences!!.getPreferencesBoolean(
                    getString(R.string.pref_user_online_pay),
                    false
                )
                && preferences!!.getPreferencesBoolean(
                    getString(R.string.pref_ispayment_gateway),
                    false
                )
            ) {
                btnInstantRenew.visibility = View.VISIBLE
            } else {
                btnInstantRenew.visibility = View.GONE
                btnAdvRenew.visibility = View.GONE
            }

            if (preferences!!.getPreferencesString(getString(R.string.pref_IsCrossMaxUnpaidInvoiceLimit)) == "false"
                && preferences!!.getPreferencesString(getString(R.string.pref_cust_type))!!
                    .contains("sharing", true)
                && preferences!!.getPreferencesBoolean(
                    getString(R.string.pref_user_online_pay),
                    false
                )
                && preferences!!.getPreferencesBoolean(
                    getString(R.string.pref_ispayment_gateway),
                    false
                )
                && dashboardResponse.planName != "No Data"
                && dashboardResponse.userStatusName == "Active"
                && dashboardResponse.advRenewCount == 0
            ) {
                btnAdvRenew.visibility = View.VISIBLE
            } else {
                btnAdvRenew.visibility = View.GONE
            }


        } else {
            if (!preferences!!.getPreferencesBoolean(
                    getString(R.string.pref_user_online_pay),
                    false
                ) && !preferences!!.getPreferencesString(getString(R.string.pref_cust_type))!!
                    .contains("sharing", true)
            ) {
                btnInstantRenew.visibility = View.GONE
                btnAdvRenew.visibility = View.GONE
            }
        }
    }

    private fun setDashboardImages(IMAGES: List<String>) {
        pager.adapter = SlidingImageAdapter(activity!!, IMAGES as ArrayList<String>)
        indicator.setViewPager(pager)
        val density = resources.displayMetrics.density
        //Set circle indicator radius
        indicator.radius = 2 * density

        handler = Handler()
        swipeTimer = Timer()

        NUM_PAGES = IMAGES.size

        // Auto start of viewpager
        update = Runnable {
            if (currentPage == NUM_PAGES) currentPage = 0
            pager.setCurrentItem(currentPage++, true)
        }

        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })
    }

    override fun onStop() {
        super.onStop()
        val fragment = activity!!.getSupportFragmentManager().getFragments();
        for (f in fragment) {
            if (f is HomeFragment) {
                println("frgment home")
            }
            println("frgment home$f")
        }
        swipeTimer.cancel()
        if (handler != null) {
            handler.removeCallbacks(update!!)
        }
        println("ondetached ")
        Log.e("fragment", "Fragmemst detached")
    }

    private fun getUserDashboard() {
        webServices!!.callWebPostService(
            getString(R.string.api_dashboard) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)),
            mutableMapOf()
        )
        webServices!!.mResponseInterface = this
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivEditProfile -> {
                startActivity(Intent(activity!!, EditProfileActivity::class.java))
            }
            R.id.btnInstantRenew -> {
                startActivity(
                    Intent(activity!!, RenewPlanActivity::class.java).putExtra(
                        "data",
                        "Instant Renew"
                    )
                )
                //startActivity(Intent(activity!!, MerchantActivity::class.java))
            }
            R.id.btnViewUsage -> {
                startActivity(Intent(activity!!, DataUsageActivity::class.java))
            }
            R.id.btnAdvRenew -> {
                startActivity(
                    Intent(activity!!, RenewPlanActivity::class.java).putExtra(
                        "data",
                        "Advance Renew"
                    )
                )
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onSuccess(methodName: String, response: String?) {
        println("method name $methodName >>>>>>> $response")
        if (isAdded) {
            preferences!!.setPreferencesBody("dashboard_response", response!!)
            val dashboardResponse = Gson().fromJson(response, DashboardResponse::class.java)
            if (dashboardResponse != null) {
                rlHome.visibility = View.VISIBLE
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_start_date),
                    dashboardResponse.planActivationDate!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_end_date),
                    dashboardResponse.planExpiryDate!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_customer_name),
                    dashboardResponse.cust!!.userId!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_ispayment_gateway),
                    dashboardResponse!!.isPaymentGateway!!
                )
                setDashboardImages(dashboardResponse!!.cust!!.dashImgList)
                setRenewButtonView(dashboardResponse)

                /*tvUserName.text = dashboardResponse.cust!!.name
            tvUserId.text = dashboardResponse.cust!!.userId
            tvUserMobile.text = dashboardResponse.cust!!.mobile
            tvUserEmail.text = dashboardResponse.cust!!.emailId*/

                val bgShape = tvPlanStatus.background as GradientDrawable
                bgShape.setStroke(
                    3,
                    ContextCompat.getColor(activity!!, R.color.primary_material_dark)
                )
                tvPlanStatus.text = dashboardResponse.userStatusName
                tvPlanStatus.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.primary_material_dark
                    )
                )
                /*tvPlanDetails.text =
                "You have ${dashboardResponse.planValidity} ${dashboardResponse.planName} ${dashboardResponse.planType} plan"
            tvPlanValidity.text =
                "Plan Activated on ${dashboardResponse.planActivationDate} & will expire on ${dashboardResponse.planExpiryDate}"*/

                val c = Calendar.getInstance().time
                val df = SimpleDateFormat("dd-MMM-yy")
                val formattedDate = df.format(c)
                tvUser.text = "Hello " + dashboardResponse.cust!!.name
                /* if (dashboardResponse.userStatusName.equals("Inactive", true)) {
                     btnAdvRenew.visibility = View.GONE
                 }*/

                var arrDate = arrayOf<String>()
                if (!dashboardResponse.planActivationDate!!.contains("No Data")) {
                    arrDate = ((activity as BaseActivity).calculateDateDifference(
                        dashboardResponse.planActivationDate!!,
                        dashboardResponse.planExpiryDate!!, formattedDate!!
                    ))
                    //}
                    tvUser.text = "Hello " + dashboardResponse.cust!!.name
                    println("array data is " + arrDate[0])

                    if (arrDate[1].toInt() >= 0) {
                        pbPlan.max = arrDate[0].toInt()
                        pbPlan.progress = arrDate[0].toInt() - arrDate[1].toInt()
                        tvPlanName.text = dashboardResponse.planName
                        tvPlanType.text = dashboardResponse.planType
                        tvPlanValidity.text = dashboardResponse.planValidity
                        tvRemainingDays.text = arrDate[1] + " Days Left"
                    } else {
                        pbPlan.max = arrDate[0].toInt()
                        pbPlan.progress = arrDate[0].toInt() - arrDate[1].toInt()
                        tvPlanName.text = dashboardResponse.planName
                        tvPlanType.text = dashboardResponse.planType
                        tvPlanValidity.text = dashboardResponse.planValidity
                        tvRemainingDays.text =
                            "Your plan is expired on " + dashboardResponse.planExpiryDate
                        tvRemainingDays.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_warning,
                            0,
                            0,
                            0
                        )
                    }

                    showChart(dashboardResponse, any_chart_view)
                } else {
                    pbPlan.max = 0
                    pbPlan.progress = 0
                    tvPlanName.text = dashboardResponse.planName
                    tvPlanType.text = dashboardResponse.planType
                    tvPlanValidity.text = dashboardResponse.planValidity
                    tvRemainingDays.text = "No Data"
                }
            } else {
                rlHome.visibility = View.GONE
            }
        }
    }

    private fun showChart(dashboardResponse: DashboardResponse?, chart: PieChart) {
        val NoOfEmp = ArrayList<Entry>();

        NoOfEmp.add(Entry(dashboardResponse!!.uploadData!!.toFloat(), 0))
        NoOfEmp.add(Entry(dashboardResponse.downloadData!!.toFloat(), 1))
        NoOfEmp.add(Entry(dashboardResponse.totalData!!.toFloat(), 2))
        val dataSet = PieDataSet(NoOfEmp, "")

        val year = ArrayList<String>()

        year.add("Uploaded Data ${dashboardResponse.uploadDataS}")
        year.add("Downloaded Data ${dashboardResponse.downloadDataS}")
        year.add("Total Data ${dashboardResponse.totalDataS}")
        /*year.add(dashboardResponse.uploadDataS!!)
        year.add(dashboardResponse.downloadDataS!!)
        year.add(dashboardResponse.totalDataS!!)*/
        /* year.add("Uploaded Data")
         year.add("Downloaded Data")
         year.add("Total Data")*/
        val data = PieData(year, dataSet)
        dataSet.colors = ColorTemplate.COLORFUL_COLORS.toMutableList()
        chart.animateXY(5000, 5000)
        dataSet.setDrawValues(false)
        chart.setDrawSliceText(false)
        chart.setDescription("")

        val legend = chart.legend
        val colorcodes = legend.colors
        legend.setComputedColors(ColorTemplate.COLORFUL_COLORS.toMutableList())
        legend.setComputedColors(ColorTemplate.COLORFUL_COLORS.toMutableList())
        legend.position = Legend.LegendPosition.BELOW_CHART_LEFT
        legend.direction = Legend.LegendDirection.LEFT_TO_RIGHT
        legend.isWordWrapEnabled = true
        legend.textSize = 16f
        val typeface = Typeface.createFromAsset(activity!!.assets, "fonts/myfont.ttf")
        legend.typeface = typeface

        /*  for (i in 0 until legend.colors.size-1) {
              val parms_left_layout = LinearLayout.LayoutParams(
                  LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
              )
              parms_left_layout.weight = 1f
              val left_layout = LinearLayout(context)
              left_layout.orientation = LinearLayout.HORIZONTAL
              left_layout.gravity = Gravity.CENTER
              left_layout.layoutParams = parms_left_layout

              val parms_legen_layout = LinearLayout.LayoutParams(
                  20, 20
              )
              parms_legen_layout.setMargins(0, 0, 20, 0)
              val legend_layout = LinearLayout(context)
              legend_layout.layoutParams = parms_legen_layout
              legend_layout.orientation = LinearLayout.HORIZONTAL
  //            legend_layout.setBackgroundColor(colorcodes[i])
              left_layout.addView(legend_layout)

              val txt_unit = TextView(context)
              txt_unit.text = legend.getLabel(i)
          }*/
        chart.data = data
        setCustomLegend(legend, year)
    }

    private fun setCustomLegend(
        l: Legend,
        year: ArrayList<String>
    ) {
        val colorcodes = l.getColors()
        for (i in 0 until l.getColors().size - 1) {
            val inflater = getLayoutInflater();
            val tr = inflater.inflate(
                R.layout.table_row_legend,
                child_layout, false
            ) as TableRow
            child_layout.addView(tr);
            val linearLayoutColorContainer = tr.getChildAt(0) as LinearLayout
            val linearLayoutColor = linearLayoutColorContainer.getChildAt(0)
            val tvLabel = tr.getChildAt(1) as TextView
            //val tvAmt =  tr.getChildAt(2)as TextView
            linearLayoutColor.setBackgroundColor(colorcodes[i])
            tvLabel.setText(l.getLabel(i))
            //tvAmt.setText(year.get(i))
        }
        any_chart_view.getLegend().setWordWrapEnabled(true)
        any_chart_view.getLegend().setEnabled(false)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
