package com.xonware.xims.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.invoices.InvoicesResponse


class DataUsageFragment : Fragment(), WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data_usage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)

        val jsonObject: MutableMap<String, String> = mutableMapOf()
        jsonObject["FromDate"] = preferences!!.getPreferencesString(getString(R.string.pref_start_date))!!
        jsonObject["ToDate"] = preferences!!.getPreferencesString(getString(R.string.pref_end_date))!!
        jsonObject["CustId"] = preferences!!.getPreferencesString(getString(R.string.pref_user_id))!!
        webServices!!.callWebPostService(getString(R.string.api_data_usage), jsonObject)
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        val invoicesResponse = Gson().fromJson(response, InvoicesResponse::class.java)

        println("response invoices "+response)
       /* var mAdapter = InvoicesAdapter(activity!!,invoicesResponse.invList as ArrayList<InvList>)
        val mLayoutManager = LinearLayoutManager(activity!!)
        rvInvoices.setLayoutManager(mLayoutManager)
        rvInvoices.setItemAnimator(DefaultItemAnimator())
        rvInvoices.setAdapter(mAdapter)*/
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
