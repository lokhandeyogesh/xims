package com.xonware.xims.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.androidnetworking.error.ANError
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.activities.EditProfileActivity
import com.xonware.xims.activities.LoginActivity
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.UpdateProfileResponse
import com.xonware.xims.response.userprofile.UserProfileResponse
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.tvUserEmail
import kotlinx.android.synthetic.main.fragment_profile.tvUserId
import kotlinx.android.synthetic.main.fragment_profile.tvUserMobile
import kotlinx.android.synthetic.main.fragment_profile.tvUserName
import org.json.JSONObject
import java.io.File

class ProfileFragment : Fragment(), WebServices.SetResponse {

    var webServices: WebServices? = null
    var preferences: Preferences? = null
    var userProfile: UserProfileResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webServices = WebServices(activity!!)
        preferences = Preferences.getInstance(activity!!)

        getUserProfile()

//        btnEdit.setOnClickListener {
//            startActivity(Intent(activity, EditProfileActivity::class.java))
//        }

        btnLogout.setOnClickListener {
            preferences!!.clearPreferences()
            startActivity(Intent(activity!!, LoginActivity::class.java))
            activity!!.finish()
        }

        ivEditProfileAvatar.setOnClickListener {
            ImagePicker.with(activity!!)
                .crop(1f, 1f)               //Crop Square image(Optional)
                .compress(1024)         //Final image size will be less than 1 MB(Optional)
                .maxResultSize(512, 512)  //Final image resolution will be less than 1080 x 1080(Optional)
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        //Image Uri will not be null for RESULT_OK
                        val fileUri = data?.data
                        ivProfileAvatar.setImageURI(fileUri)
                        //You can get File object from intent
                        val file: File = ImagePicker.getFile(data)!!
                        //You can also get File Path from intent
                        val filePath: String = ImagePicker.getFilePath(data)!!

                        println("string data is " + filePath)
                        var base64String = ((activity as BaseActivity).encodeBase64(filePath))

                        val jsonObject = JSONObject()
                        jsonObject.put("Id", userProfile!!.id.toString())
                        jsonObject.put("UserId", userProfile!!.userId.toString())
                        jsonObject.put("Base64String", base64String)
                        webServices!!.callWebJsonObjectPostService(getString(R.string.api_user_profile), jsonObject)
                        webServices!!.mResponseInterface = this

                    } else if (resultCode == ImagePicker.RESULT_ERROR) {
                        Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun getUserProfile() {
        webServices!!.callWebGetService(
            getString(R.string.api_user_profile) + "/" + preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        println("reponse " + response)
        if (isAdded) {
            when (methodName) {
                getString(R.string.api_user_profile) + "/" + preferences!!.getPreferencesString(getString(R.string.pref_user_id)) -> {
                    userProfile = Gson().fromJson(response, UserProfileResponse::class.java)
                    if (userProfile != null) {
                        val tvUserName1 = tvUserName.background as GradientDrawable
                        tvUserName1.setStroke(3, ContextCompat.getColor(activity!!, R.color.colorPrimaryDark))
                        tvUserName.text = userProfile!!.firstName?.replace("null","") + " " + userProfile!!.lastName

                        val tvUserId1 = tvUserId.background as GradientDrawable
                        tvUserId1.setStroke(3, ContextCompat.getColor(activity!!, R.color.colorPrimaryDark))
                        tvUserId.text = userProfile!!.userId

                        val tvUserMobile1 = tvUserMobile.background as GradientDrawable
                        tvUserMobile1.setStroke(3, ContextCompat.getColor(activity!!, R.color.colorPrimaryDark))
                        tvUserMobile.text = userProfile!!.mobileNo

                        val tvUserEmail1 = tvUserEmail.background as GradientDrawable
                        tvUserEmail1.setStroke(3, ContextCompat.getColor(activity!!, R.color.colorPrimaryDark))
                        tvUserEmail.text = userProfile!!.email
                    }
                }
                else -> {
                    val userProfile = Gson().fromJson(response, UpdateProfileResponse::class.java)
                    if (userProfile.status == true) {
                        ((activity as BaseActivity).showToast(userProfile.message!!))
                    } else {
                        ((activity as BaseActivity).showToast(userProfile.message!!))
                    }
                }
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
        println("error is " + error.errorCode)
    }
}
