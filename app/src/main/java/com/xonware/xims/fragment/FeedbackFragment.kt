package com.xonware.xims.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import kotlinx.android.synthetic.main.fragment_invoices.*
import java.util.ArrayList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.xonware.xims.adapter.PlanListAdapter
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.response.plan_list.Plan
import com.xonware.xims.response.plan_list.PlanListResponse


@SuppressLint("ValidFragment")
class FeedbackFragment() : BaseActivity() {

//    var webServices: WebServices? = null
//    var preferences: Preferences? = null
    lateinit var reviewInfo: ReviewInfo

//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_invoices, container, false)
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blank)

        val manager = ReviewManagerFactory.create(this)
//        val manager = FakeReviewManager(this@MainActivity)


        val request = manager.requestReviewFlow()
        request.addOnCompleteListener {
            println("it is "+it.isSuccessful)
            if (it.isSuccessful) {
                // We got the ReviewInfo object
                reviewInfo = it.result
                println("it is "+it)
                println("it is reviw result"+it.result)
                initflow(manager,reviewInfo)
            } else {
                // There was some problem, continue regardless of the result.
            }
        }
    }

    private fun initflow(manager: ReviewManager, reviewInfo: ReviewInfo) {
        val flow = manager.launchReviewFlow(this, reviewInfo)
        flow.addOnCompleteListener {
            // The flow has finished. The API does not indicate whether the user
            // reviewed or not, or even whether the review dialog was shown. Thus, no
            // matter the result, we continue our app flow.
            println("flow is "+it.result)
        }
    }
}
