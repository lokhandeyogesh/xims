package com.xonware.xims

import android.annotation.SuppressLint
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.webkit.*
import kotlinx.android.synthetic.main.activity_web_view.*
import android.webkit.ValueCallback
import android.webkit.JavascriptInterface
import org.jsoup.Jsoup
import android.R.attr
import android.app.Dialog
import android.content.Intent
import android.os.*
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import org.json.JSONObject
import org.json.JSONArray
import java.util.regex.Pattern
import java.util.regex.Pattern.DOTALL
import org.jsoup.nodes.DataNode
import java.util.Collections.replaceAll
import androidx.core.os.HandlerCompat.postDelayed
import com.instamojo.android.activities.BaseActivity
import com.xonware.xims.activities.HomeActivity
import kotlinx.android.synthetic.main.layout_payment_status.*


class WebViewActivity : BaseActivity() {

    var orderId = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        supportActionBar!!.title = "Make Payment"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val webUrl = intent.getStringExtra("url_is")
        orderId = intent.getStringExtra("order_id")

        println("URL is " + webUrl)


        val webSettings = webview_layout.settings
        webSettings.javaScriptEnabled = true
        webSettings.setSupportMultipleWindows(true)

        webview_layout.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                println("onPageFinished   " + url)
                if (url!!.contains("order/status")) {
                    /* Handler().postDelayed(Runnable {
                         //Do something after 100ms
                     }, 3000)*/
                    //onBackPressed()
                    showDialogForStatus()
                }
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                println("onPageStarted " + url)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    println("shouldOverrideUrlLoading " + request!!.url)
                }
                return false
            }
        }
        webview_layout.loadUrl(webUrl)

        webview_layout.webChromeClient = object : WebChromeClient() {

            override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsAlert " + message)
                return true
            }

            override fun onJsBeforeUnload(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsBeforeUnload " + message)
                return true
            }

            override fun onConsoleMessage(message: String?, lineNumber: Int, sourceID: String?) {
                println("onConsoleMessage " + message)
                super.onConsoleMessage(message, lineNumber, sourceID)
            }

            override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsConfirm " + message)
                return true
            }

            override fun onJsPrompt(
                view: WebView?,
                url: String?,
                message: String?,
                defaultValue: String?,
                result: JsPromptResult?
            ): Boolean {
                println("onJsPrompt " + message)
                return true
            }

            @SuppressLint("SetJavaScriptEnabled")
            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ): Boolean {
                val newWebView = WebView(this@WebViewActivity)
                newWebView.getSettings().setJavaScriptEnabled(true)
                newWebView.getSettings().setSupportZoom(true)
                newWebView.getSettings().setBuiltInZoomControls(true)
                newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
                newWebView.getSettings().setSupportMultipleWindows(true)
                view!!.addView(newWebView);
                val transport = resultMsg!!.obj as WebView.WebViewTransport
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();

                newWebView.webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view!!.loadUrl(request!!.url.toString())
                        }
                        return true
                    }
                }

                return true
            }
        }

        btn.setOnClickListener {
            println("weburl is " + webview_layout.url)
            runOnUiThread {
                val doc = Jsoup.connect(webview_layout.url).get()
                //val document = Jsoup.parse(clientToken)
                val scriptElements = doc.getElementsByTag("script")
                for (element in scriptElements) {
                    for (node in element.dataNodes()) {
                        println(node.wholeData)
                        if (node.wholeData.toString().contains("instamojoPaymentId")) {
                            println("sorted data " + node.wholeData)
                            val result = node.wholeData.toString().substringAfterLast("instamojoPaymentId")
                            val paymentStatus1 = node.wholeData.toString().substringAfterLast("paymentStatus")
                            val paymentStatus2 = paymentStatus1.substringBefore(";")
                            println("result is " + paymentStatus2)
                            val paymentId = result.replace(Regex("[^a-zA-Z0-9]"), "")
                            val paymentStatus = paymentStatus2.replace(Regex("[^a-zA-Z0-9]"), "")
                            println("result is " + paymentId)
                        }
                    }
                    println("-------------------")
                }
            }
        }
    }

    override fun onBackPressed() {
        var paymentId: String? = null
        var paymentStatus: String? = null
        runOnUiThread {
            val doc = Jsoup.connect(webview_layout.url).get()
            //val document = Jsoup.parse(clientToken)
            val scriptElements = doc.getElementsByTag("script")
            for (element in scriptElements) {
                for (node in element.dataNodes()) {
                    println(node.wholeData)
                    if (node.wholeData.toString().contains("instamojoPaymentId")) {
                        println("sorted data " + node.wholeData)
                        val result = node.wholeData.toString().substringAfterLast("instamojoPaymentId")
                        val paymentStatus1 = node.wholeData.toString().substringAfterLast("paymentStatus")
                        val paymentStatus2 = paymentStatus1.substringBefore(";")
                        println("result is " + paymentStatus2)
                        paymentId = result.replace(Regex("[^a-zA-Z0-9]"), "")
                        paymentStatus = paymentStatus2.replace(Regex("[^a-zA-Z0-9]"), "")
                    }
                }
                println("-------------------")
            }
        }
/*

        val dialog = Dialog(this@WebViewActivity)
        dialog.setContentView(R.layout.layout_payment_status)
        val btnOk1 = dialog.findViewById<TextView>(R.id.btnOk)
        dialog.show()
        if (paymentStatus.equals("success", true)) {
            btnOk1.text = "Success"
        } else {
            btnOk1.text = paymentStatus
        }
        btnOk1.setOnClickListener {
            startActivity(Intent(this@WebViewActivity, HomeActivity::class.java))
            finish()
        }
*/


        /* val intent = Intent()
         intent.putExtra(getString(R.string.insta_pay_id), paymentId)
         intent.putExtra(getString(R.string.insta_pay_status), paymentStatus)
         setResult(RESULT_OK, intent)
         finish();*/
        super.onBackPressed()
    }

    fun showDialogForStatus() {
        var paymentId: String? = null
        var paymentStatus: String? = null
        runOnUiThread {
            val doc = Jsoup.connect(webview_layout.url).get()
            //val document = Jsoup.parse(clientToken)
            val scriptElements = doc.getElementsByTag("script")
            for (element in scriptElements) {
                for (node in element.dataNodes()) {
                    println(node.wholeData)
                    if (node.wholeData.toString().contains("instamojoPaymentId")) {
                        println("sorted data " + node.wholeData)
                        val result = node.wholeData.toString().substringAfterLast("instamojoPaymentId")
                        val paymentStatus1 = node.wholeData.toString().substringAfterLast("paymentStatus")
                        val paymentStatus2 = paymentStatus1.substringBefore(";")
                        println("result is " + paymentStatus2)
                        paymentId = result.replace(Regex("[^a-zA-Z0-9]"), "")
                        paymentStatus = paymentStatus2.replace(Regex("[^a-zA-Z0-9]"), "")
                    }
                }
                println("-------------------")
            }
        }

        val dialog = Dialog(this@WebViewActivity)
        dialog.setContentView(R.layout.layout_payment_status)
        val btnOk1 = dialog.findViewById<TextView>(R.id.btnOk)
        val ivStatus = dialog.findViewById<ImageView>(R.id.ivPaystatus)
        val tvOrderId = dialog.findViewById<TextView>(R.id.tvOrderId)
        val tvTxnStatus = dialog.findViewById<TextView>(R.id.tvTxnStatus)
        tvOrderId.text = "Your Order Id is :" + orderId
        dialog.show()
        /*val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.90).toInt()
        dialog.window!!.setLayout(width, height)*/
        if (paymentStatus.equals("success", true)) {
            ivStatus.setImageResource(R.drawable.ic_checked_txnx)
            tvTxnStatus.text="Transaction was Successful !!"
            tvTxnStatus.setTextColor(ContextCompat.getColor(this@WebViewActivity,R.color.green))
        } else {
            ivStatus.setImageResource(R.drawable.ic_cancel_pay)
            tvTxnStatus.text="Transaction was Failed !!"
            tvTxnStatus.setTextColor(ContextCompat.getColor(this@WebViewActivity,R.color.red))
        }
        btnOk1.setOnClickListener {
            val intent = Intent(this@WebViewActivity, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
