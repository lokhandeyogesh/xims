package com.xonware.xims.paytm

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.paytm.pgsdk.PaytmOrder
import com.paytm.pgsdk.PaytmPGService
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.xonware.xims.R
import com.xonware.xims.activities.PaymentResponseActivity
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.CommonMessageResoponse
import com.xonware.xims.response.payment_details.PaymentDetailsResponse
import kotlinx.android.synthetic.main.activity_instamojo.*
import com.xonware.xims.response.gatewaylist.Glst
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


class PaytmActivity : BaseActivity(), WebServices.SetResponse {

    private var paymentStatus: String? = null
    private var paytmObject: Glst?=null
    var paymentDetails = PaymentDetailsResponse()
    var orderId = String()
    var merchantKey = String()
    var merchantID = String()
    var getwayId = String()
    var getwayName = String()
    lateinit var totalAmount: String
//    var transactionIdForServer = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instamojo)
        supportActionBar!!.hide()
        progressBar.visibility = View.VISIBLE
        init()
        generateOrderId()
    }

    private fun init() {
        webServices = WebServices(this@PaytmActivity)
        preferences = Preferences.getInstance(this@PaytmActivity)

        paymentDetails =
            intent.getSerializableExtra(getString(R.string.intent_payment_details)) as PaymentDetailsResponse
        paytmObject =
            intent.getSerializableExtra("data") as Glst
        merchantKey = paytmObject!!.gatewayKey!!
        merchantID = paytmObject!!.gatewayUserId!!
        getwayId = paytmObject!!.id.toString()!!
//        getwayName = paytmObject!!.gatewayName.toString()!!
        getwayName = "Paytm"
        totalAmount = paymentDetails!!.totalAmount!!.toString()

        println("payment details ${paymentDetails.taxDetailsList}")
    }

    private fun generateOrderId() {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", orderId)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", "Initiated")
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        jsonObject.put(
            "Id", preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        println("get generate order " + jsonObject)
        webServices!!.callWebGetService(
            getString(R.string.api_gen_order_id) + preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )/*, jsonObject*/
        )
        webServices!!.mResponseInterface = this
    }

    private fun setPaymentRequestData() {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", orderId)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", "Initiated")
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        jsonObject.put(
            "Id", preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        println("api_gen_order_id_request_params " + jsonObject)
        webServices!!.callWebJsonObjectPostService(
            getString(R.string.api_gen_order_id_request_params), jsonObject
        )
        webServices!!.mResponseInterface = this

    }

    override fun onSuccess(methodName: String, response: String?) {
        when (methodName) {
             getString(R.string.api_gen_order_id) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)) -> {
                if (!response.isNullOrEmpty()) {
                    orderId = response!!.replace("\"", "")
                    println("order id is " + orderId)
                   // callTokenForInstamojo()
                    setPaymentRequestData()
                    //paymentTxnStart()
                  //  generateChecksumFromServer()
//                    webServices!!.callWebGetService(
//                        "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=${client_id}&orderId=${orderId}"
//                    )
//                    webServices!!.mResponseInterface = this
                }
            }
            "Paytmchecksum" -> {
                paymentTxnStart()
            }
            getString(R.string.api_gen_order_id_request_params) -> {
                println("transaction is " + orderId)
                println("transaction is response" + response)
                paymentTxnStart()
            }
            getString(R.string.api_mark_pay_status) -> {
                val responseObject = Gson().fromJson(response, CommonMessageResoponse::class.java)
                showToast(responseObject.msg.toString())
                if (paymentStatus.equals("Credit", true)) {
                    startActivity(
                        Intent(
                            this@PaytmActivity,
                            PaymentResponseActivity::class.java
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_txn_id),
                            orderId
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_amount),
                            totalAmount
                        )
                    )
                } else {
                    startActivity(
                        Intent(
                            this@PaytmActivity,
                            PaymentResponseActivity::class.java
                        ).putExtra(getString(R.string.is_payment_success), false).putExtra(
                            getString(R.string.payment_txn_id),
                            orderId
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_amount),
                            totalAmount
                        )
                    )
                }
            }
            else->{
                println("resposnse paytm "+response)
            }
        }
    }

//    private fun generateChecksumFromServer() {
//        val jsonObject1 = JSONObject()
//        jsonObject1.put("ORDER_ID", orderId)
//        jsonObject1.put("EMAIL", "y@gmail.com")
//        jsonObject1.put("MOBILE_NO", "9876543210")
//        jsonObject1.put("TXN_AMOUNT", 20.00)
//        jsonObject1.put(
//            "CUST_ID",
//            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
//        )
//        jsonObject1.put("GetwayName", "Paytm")
//        webServices!!.callWebStringPostService("Paytmchecksum", jsonObject1)
//        webServices!!.mResponseInterface = this@PaytmActivity
//
//        //creating a retrofit object.
//        val retrofit = Retrofit.Builder()
//            .baseUrl(getString(R.string.api_base_url))
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//
//        //creating the retrofit api service
//        val apiService = retrofit.create(Api::class.java)
//
//        //creating paytm object
//        //containing all the values required
//
//        //creating a call object from the apiService
//        val call = apiService.getChecksum(
//            orderId,
//            "y@gmail.com",
//            "9876543210",
//            20,
//            preferences!!.getPreferencesString(getString(R.string.pref_user_id))!!
//        ,"Paytm"
//        )
//
//        //making the call to generate checksum
//        call.enqueue(object : Callback<String> {
//            override fun onResponse(call: Call<String>, response: Response<String>) {
//                println("retrofit call "+response.body()+call)
//              //  paymentTxnStart(response.body())
//
//                //once we get the checksum we will initiailize the payment.
//                //the method is taking the checksum we got and the paytm object as the parameter
//                //initializePaytmPayment(response.body().getChecksumHash(), paytm)
//            }
//
//            override fun onFailure(call: Call<String>, t: Throwable) {
//                println("retrofit call error "+t)
//            }
//        })
//    }

    override fun onFailure(methodName: String, error: ANError) {
        println("error is " + methodName + ">>" + error.message)
        progressBar.visibility = View.GONE
        showToast("Something went wrong try again !!")
        onBackPressed()
    }

    private fun paymentTxnStart() {
        //val service = PaytmPGService.getStagingService()
        val service = PaytmPGService.getProductionService()
        val jsonObject = TreeMap<String, String>()
        val jsonObject1 = HashMap<String, String>()

        jsonObject.put("MID", merchantID)
        jsonObject.put("ORDER_ID", orderId)
        jsonObject.put(
            "CUST_ID",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))!!
        )
        jsonObject.put("CHANNEL_ID", "WAP")
        jsonObject.put("TXN_AMOUNT", paymentDetails.totalAmount.toString())
        jsonObject.put("WEBSITE", "DEFAULT")
        //jsonObject.put("EMAIL", )
       // jsonObject.put("MOBILE_NO", "9876543210")
        jsonObject.put("INDUSTRY_TYPE_ID", "Retail109");
        jsonObject.put(
            "CALLBACK_URL",
        "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp"
        );


        jsonObject1.put("MID", merchantID)
        jsonObject1.put("ORDER_ID", orderId)
        jsonObject1.put(
            "CUST_ID",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))!!
        )
        jsonObject1.put("CHANNEL_ID", "WAP")
        jsonObject1.put("TXN_AMOUNT", paymentDetails.totalAmount.toString())
        jsonObject1.put("WEBSITE", "DEFAULT")
        jsonObject1.put("INDUSTRY_TYPE_ID", "Retail109");
        jsonObject1.put(
            "CALLBACK_URL",
        "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp"
        );

        try {
            val checkSum = CheckSumServiceHelper.getCheckSumServiceHelper()
                .genrateCheckSum(merchantKey, jsonObject)
            jsonObject1.put("CHECKSUMHASH", checkSum)
            System.out.println("Paytm Payload: $jsonObject")
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        println("jsonosbject is " + jsonObject1)

       val paytmOrder = PaytmOrder(jsonObject1)
       service.initialize(paytmOrder, null)

        service.startPaymentTransaction(
            this@PaytmActivity,
            true,
            true,
            object : PaytmPaymentTransactionCallback {
                /*Call Backs*/
                override fun someUIErrorOccurred(inErrorMessage: String) {
                    println(" someUIErrorOccurred paytm")
                    showAlertDialog("", orderId, "Some Ui Error Occured.")
                }

                override fun onTransactionResponse(inResponse: Bundle) {
                    println(" onTransactionResponse paytm $inResponse")
                    if (inResponse.getString("STATUS").toString().contains("Success",true)){
                        paymentStatus = "Credit"
                    }else{
                        paymentStatus = inResponse.getString("STATUS")
                    }
                    println("status of payment "+paymentStatus)
                    saveStatusOnServer(paymentStatus)
                }

                override fun networkNotAvailable() {
                    println(" networkNotAvailable paytm")
                    progressBar.visibility = View.GONE
                    showToast("Something went wrong try again !!")
                    onBackPressed()
                }

                override fun clientAuthenticationFailed(inErrorMessage: String) {
                    println(" clientAuthenticationFailed paytm")
                    progressBar.visibility = View.GONE
                    showToast("Client Authentication Failed.")
                    onBackPressed()
                }

                override fun onErrorLoadingWebPage(
                    iniErrorCode: Int,
                    inErrorMessage: String,
                    inFailingUrl: String
                ) {
                    println(" onErrorLoadingWebPage paytm")
                    progressBar.visibility = View.GONE
                    showToast("Error Loading Page.")
                    onBackPressed()
                }

                override fun onBackPressedCancelTransaction() {
                    println(" onBackPressedCancelTransaction paytm")
                    showAlertDialog("", orderId, "Your Last Transaction Was Cancelled.")
                }

                override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle) {
                    println(" onTransactionCancel paytm")
                    showAlertDialog("", orderId, "Your Last Transaction Was Cancelled.")
                }
            })
    }

    private fun saveStatusOnServer(paymentStatus: String?) {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", orderId)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", paymentStatus)
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        println("jsonoject is " + jsonObject)
        webServices!!.callWebJsonObjectPostService(
            getString(R.string.api_mark_pay_status),
            jsonObject
        )
        webServices!!.mResponseInterface = this
    }

}
