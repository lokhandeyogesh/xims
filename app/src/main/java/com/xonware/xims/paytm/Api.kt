package com.xonware.xims.paytm

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.POST
import retrofit2.http.FormUrlEncoded


interface Api {

    @FormUrlEncoded
    @POST("Paytmchecksum")
    fun getChecksum(
        @Field("ORDER_ID") orderId: String,
        @Field("EMAIL") email: String,
        @Field("MOBILE_NO") mobile: String,
        @Field("TXN_AMOUNT") txnAmount: Any,
        @Field("CUST_ID") custid: String,
        @Field("GetwayName") getwayname: String
//        @Field("INDUSTRY_TYPE_ID") industryTypeId: String
    ): Call<String>

    companion object {

        //this is the URL of the paytm folder that we added in the server
        //make sure you are using your ip else it will not work
        val BASE_URL = "http://192.168.101.1/paytm/"
    }

}