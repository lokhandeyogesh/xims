package com.xonware.xims.response.plan_list

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlanNew {

    @SerializedName("PlanCat")
    @Expose
    var planCat: String? = null

    override fun toString(): String {
        return planCat!!
    }

    @SerializedName("Plan")
    @Expose
    var plan: List<Plan>? = null

}
