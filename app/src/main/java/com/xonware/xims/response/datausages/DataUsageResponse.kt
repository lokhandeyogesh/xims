package com.xonware.xims.response.datausages

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DataUsageResponse {

    @SerializedName("Upload")
    @Expose
    var upload: String? = null
    @SerializedName("Download")
    @Expose
    var download: String? = null
    @SerializedName("Total")
    @Expose
    var total: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("EmailId")
    @Expose
    var emailId: String? = null
    @SerializedName("UserId")
    @Expose
    var userId: String? = null
    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("FromDate")
    @Expose
    var fromDate: String? = null
    @SerializedName("ToDate")
    @Expose
    var toDate: String? = null
    @SerializedName("dataList")
    @Expose
    var dataList: List<DataList>? = null
    @SerializedName("ExtraUsageTotal")
    @Expose
    var extraUsageTotal: Any? = null
    @SerializedName("CustShiftId")
    @Expose
    var custShiftId: Any? = null
    @SerializedName("PlanName")
    @Expose
    var planName: Any? = null
    @SerializedName("MACID")
    @Expose
    var macid: Any? = null
    @SerializedName("AcctTerminationCause")
    @Expose
    var acctTerminationCause: Any? = null
    @SerializedName("PreviousUrl")
    @Expose
    var previousUrl: Any? = null

}
