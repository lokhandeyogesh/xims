package com.xonware.xims.response.plan_list

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlanListResponse {

    @SerializedName("PlanNew")
    @Expose
    var planNew: List<PlanNew>? = null

}
