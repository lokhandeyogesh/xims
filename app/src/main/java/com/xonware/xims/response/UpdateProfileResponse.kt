package com.xonware.xims.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateProfileResponse {

    @SerializedName("Status")
    @Expose
    var status: Boolean? = null
    @SerializedName("Message")
    @Expose
    var message: String? = null
    @SerializedName("UniqueId")
    @Expose
    var uniqueId: Int? = null
    @SerializedName("CustType")
    @Expose
    var custType: Any? = null
    @SerializedName("UserRenewaltype")
    @Expose
    var userRenewaltype: Any? = null
    @SerializedName("DisplayPlanList")
    @Expose
    var displayPlanList: Any? = null
    @SerializedName("UserOnlinePayment")
    @Expose
    var userOnlinePayment: Any? = null
    @SerializedName("Ticket")
    @Expose
    var ticket: Any? = null
    @SerializedName("SMSSend")
    @Expose
    var smsSend: Any? = null
    @SerializedName("DisplayInvoice")
    @Expose
    var displayInvoice: Any? = null

}
