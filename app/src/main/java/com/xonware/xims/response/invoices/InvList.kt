package com.xonware.xims.response.invoices

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InvList {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("TransactionId")
    @Expose
    var transactionId: String? = null
    @SerializedName("TransactionDate")
    @Expose
    var transactionDate: String? = null
    @SerializedName("InvoiceNo")
    @Expose
    var invoiceNo: String? = null
    @SerializedName("PlanName")
    @Expose
    var planName: String? = null
    @SerializedName("PlanValidity")
    @Expose
    var planValidity: String? = null
    @SerializedName("PaymentMode")
    @Expose
    var paymentMode: String? = null
    @SerializedName("Amount")
    @Expose
    var amount: String? = null
    @SerializedName("PaymentStatus")
    @Expose
    var paymentStatus: String? = null
    @SerializedName("PaymentType")
    @Expose
    var paymentType: Any? = null
    @SerializedName("IsDelete")
    @Expose
    var isDelete: Boolean? = null
    @SerializedName("CreateByName")
    @Expose
    var createByName: Any? = null
    @SerializedName("LastModifiedByName")
    @Expose
    var lastModifiedByName: Any? = null
    @SerializedName("CreateDate")
    @Expose
    var createDate: Any? = null
    @SerializedName("LastModifiedDate")
    @Expose
    var lastModifiedDate: Any? = null
    @SerializedName("TransactionDates")
    @Expose
    var transactionDates: String? = null
    @SerializedName("UserId")
    @Expose
    var userId: Any? = null
    @SerializedName("ChkList")
    @Expose
    var chkList: Boolean? = null
    @SerializedName("InvoiceStatus")
    @Expose
    var invoiceStatus: String? = null
    @SerializedName("PayByStatus")
    @Expose
    var payByStatus: Any? = null

}
