package com.xonware.xims.response.tickets

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TicketResponse {

    @SerializedName("STList")
    @Expose
    var stList: List<STList>? = null

}
