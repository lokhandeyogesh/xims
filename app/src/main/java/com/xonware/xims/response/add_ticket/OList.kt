package com.xonware.xims.response.add_ticket

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OList {

    @SerializedName("ID")
    @Expose
    var id: Int? = null
    @SerializedName("OptionS")
    @Expose
    var optionS: String? = null

}
