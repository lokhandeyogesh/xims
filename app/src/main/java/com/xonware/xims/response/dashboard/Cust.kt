package com.xonware.xims.response.dashboard

import java.util.ArrayList
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Cust {

    @SerializedName("UserId")
    @Expose
    var userId: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("Address")
    @Expose
    var address: String? = null
    @SerializedName("EmailId")
    @Expose
    var emailId: String? = null
    @SerializedName("Mobile")
    @Expose
    var mobile: String? = null
    @SerializedName("Photo")
    @Expose
    var photo: String? = null
    @SerializedName("DashImgList")
    @Expose
    var dashImgList: List<String> = ArrayList()

}
