package com.xonware.xims.response.payment_details

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.xonware.xims.response.tax_response.Taxlst
import java.io.Serializable

class PaymentDetailsResponse:Serializable {

    @SerializedName("CustId")
    @Expose
    var custId: Int? = null
    @SerializedName("PlanId")
    @Expose
    var planId: Int? = null
    @SerializedName("PlanCategoryId")
    @Expose
    var planCategoryId: Int? = null
    @SerializedName("BillingCycleId")
    @Expose
    var billingCycleId: Int? = null
    @SerializedName("ZoneId")
    @Expose
    var zoneId: Int? = null
    @SerializedName("UserId")
    @Expose
    var userId: String? = null
    @SerializedName("PlanAmount")
    @Expose
    var planAmount: Float? = null
    @SerializedName("PlanDiscountAmount")
    @Expose
    var planDiscountAmount: Any? = null
    @SerializedName("FinalAmount")
    @Expose
    var finalAmount: Float? = null
    @SerializedName("TotalAmount")
    @Expose
    var totalAmount: Float? = null
    @SerializedName("CouponCode")
    @Expose
    var couponCode: Any? = null
    @SerializedName("CouponId")
    @Expose
    var couponId: Float? = null
    @SerializedName("taxDetailsList")
    @Expose
    var taxDetailsList: List<Taxlst>? = null
    @SerializedName("PlanMasterId")
    @Expose
    var planMasterId: Float? = null
    @SerializedName("PlanMasterDetailsId")
    @Expose
    var planMasterDetailsId: Int? = null
    @SerializedName("CouponDiscountAmount")
    @Expose
    var couponDiscountAmount: Any? = null
    @SerializedName("CouponMessage")
    @Expose
    var couponMessage: Any? = null
    @SerializedName("PlanName")
    @Expose
    var planName: String? = null
    @SerializedName("BillingCycleName")
    @Expose
    var billingCycleName: String? = null
    @SerializedName("PlanCategoryName")
    @Expose
    var planCategoryName: String? = null
    @SerializedName("TotalTaxAmount")
    @Expose
    var totalTaxAmount: Float? = null
    @SerializedName("gatewayList")
    @Expose
    var gatewayList: List<GatewayList>? = null
    @SerializedName("PayOnline")
    @Expose
    var payOnline: Double? = null
    @SerializedName("PaymentType")
    @Expose
    var paymentType: String? = null
    @SerializedName("Userflg")
    @Expose
    var userflg: Boolean? = null
    @SerializedName("InstallationAmount")
    @Expose
    var installationAmount: Double? = null
    @SerializedName("CurrentWalletBalance")
    @Expose
    var currentWalletBalance: Any? = null
    @SerializedName("ExpDate")
    @Expose
    var expDate: String? = null
    @SerializedName("CouponEnable")
    @Expose
    var couponEnable: Any? = null

}
