package com.xonware.xims.response.dashboard

import java.util.ArrayList
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DashboardResponse {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("PlanName")
    @Expose
    var planName: String? = null
    @SerializedName("PlanType")
    @Expose
    var planType: String? = null
    @SerializedName("PlanValidity")
    @Expose
    var planValidity: String? = null
    @SerializedName("PlanActivationDate")
    @Expose
    var planActivationDate: String? = null
    @SerializedName("PlanExpiryDate")
    @Expose
    var planExpiryDate: String? = null
    @SerializedName("UserStatusName")
    @Expose
    var userStatusName: String? = null
    @SerializedName("UploadData")
    @Expose
    var uploadData: Double? = null
    @SerializedName("DownloadData")
    @Expose
    var downloadData: Double? = null
    @SerializedName("UploadDataS")
    @Expose
    var uploadDataS: String? = null
    @SerializedName("DownloadDataS")
    @Expose
    var downloadDataS: String? = null
    @SerializedName("TotalDataS")
    @Expose
    var totalDataS: String? = null
    @SerializedName("TotalData")
    @Expose
    var totalData: Double? = null
    @SerializedName("StaticIP")
    @Expose
    var staticIP: Any? = null
    @SerializedName("cust")
    @Expose
    var cust: Cust? = null
    @SerializedName("onlineUserData")
    @Expose
    var onlineUserData: List<Any> = ArrayList()
    @SerializedName("ticketData")
    @Expose
    var ticketData: List<Any> = ArrayList()
    @SerializedName("advRenewCount")
    @Expose
    var advRenewCount: Int? = null
    @SerializedName("ErrorMessage")
    @Expose
    var errorMessage: Any? = null
    @SerializedName("IsPaymentGateway")
    @Expose
    var isPaymentGateway: Boolean? = null
    @SerializedName("TicketEnable")
    @Expose
    var ticketEnable: Boolean? = null
    @SerializedName("UserOnlinePayment")
    @Expose
    var userOnlinePayment: Boolean? = null
    @SerializedName("ConnectionStatus")
    @Expose
    var connectionStatus: Boolean? = null

}
