package com.xonware.xims.response.plan_list

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Plan {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("PlanName")
    @Expose
    var planName: String? = null
    @SerializedName("PlanCategory")
    @Expose
    var planCategory: String? = null
    @SerializedName("PlanDataspeed")
    @Expose
    var planDataspeed: String? = null
    @SerializedName("PlanType")
    @Expose
    var planType: Int? = null
    @SerializedName("Days")
    @Expose
    var days: Any? = null
    @SerializedName("PDlst")
    @Expose
    var pDlst: List<PDlst>? = null

    override fun toString(): String {
        return planName!!
    }

}
