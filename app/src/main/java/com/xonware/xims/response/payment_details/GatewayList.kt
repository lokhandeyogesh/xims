package com.xonware.xims.response.payment_details

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GatewayList:Serializable {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("GatewayName")
    @Expose
    var gatewayName: String? = null
    @SerializedName("GatewayTypeName")
    @Expose
    var gatewayTypeName: String? = null

}
