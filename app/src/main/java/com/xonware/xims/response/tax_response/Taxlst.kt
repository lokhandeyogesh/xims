package com.xonware.xims.response.tax_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Taxlst :Serializable{

    @SerializedName("TaxName")
    @Expose
    var taxName: String? = null
    //userDefined
    @SerializedName("TaxAmount")
    @Expose
    var taxAnount: Double? = null
    @SerializedName("TaxPercent")
    @Expose
    var taxPercent: Double? = null

}
