package com.xonware.xims.response.plan_list

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PDlst {

    @SerializedName("Pid")
    @Expose
    var pid: Int? = null
    @SerializedName("Billingcycle")
    @Expose
    var billingcycle: String? = null
    @SerializedName("BillingId")
    @Expose
    var BillingId: String? = null
    @SerializedName("Amount")
    @Expose
    var amount: Float? = null

    override fun toString(): String {
        return billingcycle!!
    }

}
