package com.xonware.xims.response.userprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserProfileResponse {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("UserId")
    @Expose
    var userId: String? = null
    @SerializedName("LastName")
    @Expose
    var lastName: String? = null
    @SerializedName("FirstName")
    @Expose
    var firstName: String? = null
    @SerializedName("MobileNo")
    @Expose
    var mobileNo: String? = null
    @SerializedName("Email")
    @Expose
    var email: String? = null
    @SerializedName("UserImagePath")
    @Expose
    var userImagePath: String? = null
    @SerializedName("NewPassword")
    @Expose
    var newPassword: Any? = null
    @SerializedName("CnfirmPassword")
    @Expose
    var cnfirmPassword: Any? = null
    @SerializedName("OriginalPassword")
    @Expose
    var originalPassword: String? = null
    @SerializedName("Base64String")
    @Expose
    var base64String: Any? = null

}
