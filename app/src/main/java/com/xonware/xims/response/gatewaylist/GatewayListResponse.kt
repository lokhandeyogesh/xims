package com.xonware.xims.response.gatewaylist

import java.util.ArrayList
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GatewayListResponse :Serializable{

    @SerializedName("glst")
    @Expose
    var glst: List<Glst> = ArrayList()

}
