package com.xonware.xims.response.invoices

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InvoicesResponse {

    @SerializedName("InvList")
    @Expose
    var invList: List<InvList>? = null

}
