package com.xonware.xims.response.add_ticket

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddTicketResponse {

    @SerializedName("CustId")
    @Expose
    var custId: Int? = null
    @SerializedName("User")
    @Expose
    var user: Any? = null
    @SerializedName("TicketDesc")
    @Expose
    var ticketDesc: Any? = null
    @SerializedName("OList")
    @Expose
    var oList: List<OList>? = null
    @SerializedName("SelectedOption")
    @Expose
    var selectedOption: Int? = null
    @SerializedName("UserChk")
    @Expose
    var userChk: Boolean? = null

}
