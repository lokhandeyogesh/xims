package com.xonware.xims.response.datausages

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DataList {

    @SerializedName("BytesIn")
    @Expose
    var bytesIn: String? = null
    @SerializedName("BytesOut")
    @Expose
    var bytesOut: String? = null
    @SerializedName("ByteTotal")
    @Expose
    var byteTotal: String? = null
    @SerializedName("CallerId")
    @Expose
    var callerId: String? = null
    @SerializedName("IPAddress")
    @Expose
    var ipAddress: String? = null
    @SerializedName("SessionTime")
    @Expose
    var sessionTime: String? = null
    @SerializedName("SessionEndDate")
    @Expose
    var sessionEndDate: String? = null
    @SerializedName("SessionId")
    @Expose
    var sessionId: Any? = null
    @SerializedName("SessionStartDate")
    @Expose
    var sessionStartDate: String? = null
    @SerializedName("AcctTerminationCause")
    @Expose
    var acctTerminationCause: Any? = null
    @SerializedName("SessionEndDateS")
    @Expose
    var sessionEndDateS: String? = null
    @SerializedName("SessionStartDateS")
    @Expose
    var sessionStartDateS: String? = null
    @SerializedName("Id")
    @Expose
    var id: Int? = null

}
