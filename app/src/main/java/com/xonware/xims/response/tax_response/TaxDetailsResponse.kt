package com.xonware.xims.response.tax_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TaxDetailsResponse {

    @SerializedName("Taxlst")
    @Expose
    var taxlst: List<Taxlst>? = null

}
