package com.xonware.xims.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse {

    @SerializedName("Status")
    @Expose
    var status: Boolean? = null
    @SerializedName("Message")
    @Expose
    var message: String? = null
    @SerializedName("UniqueId")
    @Expose
    var uniqueId: Int? = null
    @SerializedName("CustType")
    @Expose
    var custType: String? = null
    @SerializedName("IsCrossMaxUnpaidInvoiceLimit")
    @Expose
    var IsCrossMaxUnpaidInvoiceLimit: String? = null
    @SerializedName("UserRenewaltype")
    @Expose
    var userRenewaltype: Boolean? = null
    @SerializedName("DisplayPlanList")
    @Expose
    var displayPlanList: Boolean? = null
    @SerializedName("UserOnlinePayment")
    @Expose
    var userOnlinePayment: Boolean? = null
    @SerializedName("Ticket")
    @Expose
    var ticket: Boolean? = null
    @SerializedName("DisplayInvoice")
    @Expose
    var displayInvoice: Boolean? = null
}
