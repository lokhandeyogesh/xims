package com.xonware.xims.response.gatewaylist

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Glst :Serializable{

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("GatewayName")
    @Expose
    var gatewayName: String? = null
    @SerializedName("GatewayUserId")
    @Expose
    var gatewayUserId: String? = null
    @SerializedName("GatewayKey")
    @Expose
    var gatewayKey: String? = null
    @SerializedName("GatewayPort")
    @Expose
    var gatewayPort: Any? = null
    @SerializedName("GatewayRedirectUrl")
    @Expose
    var gatewayRedirectUrl: Any? = null
    @SerializedName("GatewayReturnUrl")
    @Expose
    var gatewayReturnUrl: String? = null

}
