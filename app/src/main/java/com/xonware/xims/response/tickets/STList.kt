package com.xonware.xims.response.tickets

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class STList {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("TicketNo")
    @Expose
    var ticketNo: String? = null
    @SerializedName("TicketDate")
    @Expose
    var ticketDate: String? = null
    @SerializedName("SupportTicketOptionsId")
    @Expose
    var supportTicketOptionsId: Int? = null
    @SerializedName("STicketOption")
    @Expose
    var sTicketOption: String? = null
    @SerializedName("SupportCategoryId")
    @Expose
    var supportCategoryId: Int? = null
    @SerializedName("SupportCategory")
    @Expose
    var supportCategory: String? = null
    @SerializedName("SupportStatusId")
    @Expose
    var supportStatusId: Int? = null
    @SerializedName("SupportStatus")
    @Expose
    var supportStatus: String? = null
    @SerializedName("Comment")
    @Expose
    var comment: String? = null
    @SerializedName("UserId")
    @Expose
    var userId: String? = null
    @SerializedName("User")
    @Expose
    var user: String? = null
    @SerializedName("TicketDesc")
    @Expose
    var ticketDesc: String? = null
    @SerializedName("CreateByName")
    @Expose
    var createByName: String? = null
    @SerializedName("TicketDateS")
    @Expose
    var ticketDateS: Any? = null
    @SerializedName("EngName")
    @Expose
    var engName: Any? = null
    @SerializedName("AssignDate")
    @Expose
    var assignDate: Any? = null

}
