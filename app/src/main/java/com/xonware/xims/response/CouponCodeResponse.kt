package com.xonware.xims.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CouponCodeResponse {

    @SerializedName("UserId")
    @Expose
    var userId: Int? = null
    @SerializedName("CouponCode")
    @Expose
    var couponCode: String? = null
    @SerializedName("CouponType")
    @Expose
    var couponType: String? = null
    @SerializedName("CouponValue")
    @Expose
    var couponValue: String? = null
    @SerializedName("Msg")
    @Expose
    var msg: String? = null

}
