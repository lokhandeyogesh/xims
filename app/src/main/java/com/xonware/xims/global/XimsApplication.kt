package com.xonware.xims.global

import android.app.Application
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient

class XimsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Adding an Network Interceptor for Debugging purpose :
        val okHttpClient = OkHttpClient().newBuilder()
            //.addNetworkInterceptor(StethoInterceptor())
            .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)
    }
}