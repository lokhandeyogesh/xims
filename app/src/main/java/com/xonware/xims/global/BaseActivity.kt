package com.xonware.xims.global

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.PersistableBundle
import android.text.TextUtils
import android.util.Base64
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("Registered")
open class BaseActivity() : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {

    private val connectionBroadcast = ConnectivityReceiver()
    var isOnline: Boolean = false
    var webServices: WebServices? = null
    var preferences: Preferences? = null
    val myCalendar = Calendar.getInstance()
    var returnDate: String? = null
    var builder: AlertDialog.Builder? = null

    fun showToast(message: String) {
        Toast.makeText(this@BaseActivity, message, Toast.LENGTH_LONG).show()
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
        registerReceiver(
            connectionBroadcast,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        webServices = WebServices(this)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isOnline = isConnected
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            unregisterReceiver(connectionBroadcast)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun isValidMail(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isValidMobile(phone: String): Boolean {
        return if (phone.length < 10) {
            false
        } else {
            android.util.Patterns.PHONE.matcher(phone).matches()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateFromString(input: String): String {
        val inputFormatter = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss")
        val date = inputFormatter.parse(input)

        val outputFormatter = SimpleDateFormat("dd-MMM-yyyy")
        return outputFormatter.format(date!!)
    }

    fun encodeBase64(str: String): String? {
        val bm = BitmapFactory.decodeFile(str)
        val babs = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, babs) //bm is the bitmap object
        val b = babs.toByteArray()
        val base64 = Base64.encodeToString(b, Base64.DEFAULT)
        return base64
    }

    fun decodeBase64(base64: String): String {
        val dataset = Base64.decode(base64, Base64.DEFAULT)
        val str = String(dataset, charset("UTF-8"))
        return str
    }

    @SuppressLint("SimpleDateFormat")
    fun calculateDateDifference(dateStart: String, dateStop: String, currentDate: String): Array<String> {
        //HH converts hour in 24 hours format (0-23), day calculation
        val format = SimpleDateFormat("dd-MMM-yy")

        var d1: Date? = null
        var d2: Date? = null
        var d3: Date? = null

        /*d1 = format.parse("14-Jan-2019")
        d2 = format.parse("31-March-2019")*/

        d1 = format.parse(dateStart)
        d2 = format.parse(dateStop)
        d3 = format.parse(currentDate)

        //in milliseconds
        val totalDays = d2!!.time - d1!!.time
        val remainingDays = d2!!.time - d3!!.time

        val diffSeconds = totalDays / 1000 % 60
        val diffMinutes = totalDays / (60 * 1000) % 60
        val diffHours = totalDays / (60 * 60 * 1000) % 24
        val diffDays = totalDays / (24 * 60 * 60 * 1000)
        val remainDays = remainingDays / (24 * 60 * 60 * 1000)


        println("$diffDays days, ")
        println("$remainDays remaining days, ")
        println("$totalDays total days, ")
        println("$diffHours hours, ")
        println("$diffMinutes minutes, ")
        println("$diffSeconds seconds.")

        return arrayOf(diffDays.toString(), remainDays.toString())
    }

    fun showAlertDialog(transactionId: String, orderId: String, title: String) {
        builder = AlertDialog.Builder(this)
        builder!!.setTitle(title)
        if (!transactionId.isNullOrEmpty()) {
            builder!!.setMessage("Transaction Id: ${transactionId}\nOrder Id: ${orderId}")
        } else {
            builder!!.setMessage("Order Id: ${orderId}")
        }
            .setCancelable(false)
            .setPositiveButton("ok") { _, _ ->
                finish()
                onBackPressed()
            }
        val alert = builder!!.create()
        alert.show()
    }
}