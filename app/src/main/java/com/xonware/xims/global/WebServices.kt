package com.xonware.xims.global

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.net.Uri
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener
import com.bumptech.glide.Glide
import com.xonware.xims.R
import org.json.JSONObject


class WebServices(val context: Context) {

    lateinit var mResponseInterface: SetResponse

    interface SetResponse {
        fun onSuccess(methodName: String, response: String?)
        fun onFailure(methodName: String, error: ANError)
    }

    fun callWebPostService(methodName: String, jsonObject: MutableMap<String, String>) {

        println("jsonobject is " + jsonObject)
        var url: String
        if (methodName.contains("instamojo")) {
            url = methodName
        } else {
            url = context.getString(R.string.api_base_url) + methodName
        }
        val newUri = buildURI(url, jsonObject)
        println("url is " + newUri)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.post(newUri.toString())
            .setTag(methodName)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)
                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.errorDetail)
                    progressDialog.dismiss()
                    if (anError!!.errorDetail.contains("connectionError")) {
                        val dialog = Dialog(context, R.style.AppTheme)
                        dialog.setContentView(R.layout.layout_no_connection)
                        val btnOk1 = dialog.findViewById<TextView>(R.id.btnClick)
                        val ivSmall = dialog.findViewById<ImageView>(R.id.ivSmall)
                        val tbDialog = dialog.findViewById<Toolbar>(R.id.tbDialog)
                        tbDialog.title = "Connectivity Lost"
                        Glide.with(context).asGif().load(R.drawable.wi_fi).into(ivSmall)
                        tbDialog.setNavigationOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()
                        dialog.window!!.setLayout(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                        btnOk1.setOnClickListener {
                            callWebPostService(methodName, jsonObject)
                            dialog.dismiss()
                        }
                    } else {
                        mResponseInterface.onFailure(methodName, anError!!)
                    }
                }
            })
    }

    fun callWebPostServiceWithHeaders(methodName: String, jsonObject: JSONObject, string: String) {
        println("jsonobject is $jsonObject")
        var url: String
        if (methodName.contains("instamojo")) {
            url = methodName
        } else {
            url = context.getString(R.string.api_base_url) + methodName
        }
        //val newUri = buildURI(url, jsonObject)
        println("headers " + "Bearer $string")
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.post(url)
            .addJSONObjectBody(jsonObject)
            .addHeaders("Content-Type", "application/x-www-form-urlencoded")
            .addHeaders("Authorization", "Bearer $string")
            .setTag(methodName)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.errorDetail)
                    progressDialog.dismiss()
                    if (anError!!.errorDetail.contains("connectionError")) {

                        val dialog = Dialog(context, R.style.AppTheme)
                        dialog.setContentView(R.layout.layout_no_connection)
                        val btnOk1 = dialog.findViewById<TextView>(R.id.btnClick)
                        val ivSmall = dialog.findViewById<ImageView>(R.id.ivSmall)
                        val tbDialog = dialog.findViewById<Toolbar>(R.id.tbDialog)
                        tbDialog.title = "Connectivity Lost"
                        Glide.with(context).asGif().load(R.drawable.wi_fi).into(ivSmall)
                        tbDialog.setNavigationOnClickListener {
                            //onBackPressed()
                            dialog.dismiss()
                        }
                        dialog.show()
                        /* val width = (context.resources.displayMetrics.widthPixels * 1)
                         val height = (context.resources.displayMetrics.heightPixels * 1)*/
                        dialog.window!!.setLayout(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                        btnOk1.setOnClickListener {
                            // callWebPostService(methodName, jsonObject)
                            dialog.dismiss()
                        }
                    } else {
                        mResponseInterface.onFailure(methodName, anError!!)
                    }
                }
            })
    }

    private fun buildURI(url: String, params: Map<String, String>): Uri {
        // build url with parameters.
        val builder = Uri.parse(url).buildUpon()
        for ((key, value) in params) {
            builder.appendQueryParameter(key, value)
        }
        return builder.build()
    }

    fun callWebGetService(methodName: String) {
        var url: String? = null
        if (methodName.contains(context.getString(R.string.api_load_invoice))) {
            url = "https://xims.xonware.com/User/" + methodName
        } else if (methodName.contains("paytm")) {
            url = methodName
        } else {
            url = context.getString(R.string.api_base_url) + methodName
        }
        println("url is " + url)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.get(url)
            .setTag(methodName)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    try {
                        progressDialog.dismiss()
                    }catch (e:Exception){
                        e.printStackTrace()
                    }
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.message)
                    progressDialog.dismiss()
                    mResponseInterface.onFailure(methodName, anError!!)
                }
            })
    }


    fun callWebGetServiceInstamojo(methodName: String, header: String) {
        var url: String? = methodName

        println("url is " + url + ">>>   " + header)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.get(url)
            .setTag(methodName)
            .addHeaders("Authorization", header)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.message)
                    progressDialog.dismiss()
                    mResponseInterface.onFailure(methodName, anError!!)
                }
            })
    }

    fun callWebGetFormEncodeService(methodName: String, jsonObject: JSONObject, token: String?) {
        var url: String? = methodName
        /*  if (methodName.contains(context.getString(R.string.api_load_invoice))) {
              url = "https://xims.xonware.com/User/" + methodName
          } else {
              url = context.getString(R.string.api_base_url) + methodName
          }*/
        println("url is " + jsonObject + ">>>>    " + url + ">>>>> " + token)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.post(url)
            .addJSONObjectBody(jsonObject)
            .setTag(methodName)
            .addHeaders("Content-Type", "application/json")
            .addHeaders("Authorization", "Bearer " + token)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.message)
                    progressDialog.dismiss()
                    mResponseInterface.onFailure(methodName, anError!!)
                }
            })
    }

    fun callWebJsonObjectPostService(methodName: String, jsonObject: JSONObject) {
        var url: String? = null
        if (methodName.contains("instamojo")) {
            url = methodName
        } else {
            url = context.getString(R.string.api_base_url) + methodName
        }
        println("url is " + url)
        println("url is " + jsonObject)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.post(url)
            .addJSONObjectBody(jsonObject)
            .setTag(methodName)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.response)
                    progressDialog.dismiss()
                    mResponseInterface.onFailure(methodName, anError!!)
                }
            })
    }


    fun callWebStringPostService(methodName: String, jsonObject: JSONObject) {
        var url: String? = null
        if (methodName.contains("instamojo")) {
            url = methodName
        } else {
            url = context.getString(R.string.api_base_url) + methodName
        }
        println("url is " + url)
        println("url is " + jsonObject)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        AndroidNetworking.post(url)
            .addJSONObjectBody(jsonObject)
            .setTag(methodName)
            .setPriority(Priority.HIGH)
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    println("success response is $response")
                    progressDialog.dismiss()
                    mResponseInterface.onSuccess(methodName, response!!)

                }

                override fun onError(anError: ANError?) {
                    println("error response is " + anError!!.response)
                    progressDialog.dismiss()
                    mResponseInterface.onFailure(methodName, anError!!)
                }
            })

    }
}
