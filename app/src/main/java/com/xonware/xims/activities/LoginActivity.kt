package com.xonware.xims.activities

import android.content.Intent
import android.os.Bundle
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.LoginResponse
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), WebServices.SetResponse {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_new)
        supportActionBar!!.hide()
        webServices = WebServices(this@LoginActivity)
        preferences = Preferences.getInstance(this@LoginActivity)
        if (!preferences!!.getPreferencesString(getString(R.string.pref_user_id)).isNullOrEmpty()) {
            startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
            finish()
        }
        tvForgotPass.setOnClickListener {
            startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
        }

        btnSignIn.setOnClickListener {
            if (!isOnline) {
                showToast("Please connect to internet !")
            } else {
                val username = etUserId.text.toString().trim()
                val password = etLoginPass.text.toString().trim()
                if (username.isEmpty()) {
                    showToast("Enter userId")
                } else if (password.isEmpty()) {
                    showToast("Enter password")
                } else if (username.isEmpty() && password.isEmpty()) {
                    showToast("Enter UserId and password")
                } else {
                    val jsonObject: MutableMap<String, String> = mutableMapOf()
                    jsonObject["UserId"] = username
                    jsonObject["Password"] = password
                    webServices!!.callWebPostService(getString(R.string.api_login), jsonObject)
                    webServices!!.mResponseInterface = this
                }
            }
        }
    }

    override fun onSuccess(methodName: String, response: String?) {
        if (!response.isNullOrEmpty()) {
            val loginResponse = Gson().fromJson(response, LoginResponse::class.java)
            if (loginResponse!!.status!!) {
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_user_id),
                    loginResponse.uniqueId!!.toString()
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_user_type),
                    loginResponse.custType!!.toString()
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_display_invoice),
                    loginResponse.displayInvoice!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_display_plan),
                    loginResponse.displayPlanList!!
                )
                if (loginResponse.IsCrossMaxUnpaidInvoiceLimit != null) {
                    preferences!!.setPreferencesBody(
                        getString(R.string.pref_IsCrossMaxUnpaidInvoiceLimit),
                        loginResponse.IsCrossMaxUnpaidInvoiceLimit!!
                    )
                }
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_user_renewal),
                    loginResponse.userOnlinePayment!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_cust_type),
                    loginResponse.custType!!.toLowerCase()
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_user_online_pay),
                    loginResponse.userOnlinePayment!!
                )
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_show_tickets),
                    loginResponse.ticket!!
                )
                showToast(loginResponse.message!!)
                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                finish()
            } else {
                showToast(loginResponse.message!!)
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
