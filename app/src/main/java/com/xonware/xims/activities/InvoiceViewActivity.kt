package com.xonware.xims.activities

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Message
import android.os.StrictMode
import android.print.PrintAttributes
import android.print.PrintManager
import android.view.MenuItem
import android.view.View
import android.webkit.*
import com.instamojo.android.activities.BaseActivity
import com.xonware.xims.R
import kotlinx.android.synthetic.main.activity_web_view.*


class InvoiceViewActivity : BaseActivity() {

    var invoice_id = String()

    @SuppressLint("SetJavaScriptEnabled", "RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        webview_layout.visibility = View.GONE
        fab_download.visibility = View.GONE
        progressInvoice.visibility = View.VISIBLE

        supportActionBar!!.title = "Invoice"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        invoice_id = intent.getStringExtra("invoice_id")

        val webUrl = getString(R.string.api_base_url).replace("api/","")+"user/" + getString(R.string.api_load_invoice) + invoice_id

        val webSettings = webview_layout.settings
        webSettings.javaScriptEnabled = true
        webSettings.setSupportMultipleWindows(true)
        webview_layout.settings.loadWithOverviewMode = true
        webview_layout.settings.useWideViewPort = true
        webview_layout.settings.builtInZoomControls = true

        fab_download.setOnClickListener {
            createWebPrintJob(webview_layout)
        }

        webview_layout.webViewClient = object : WebViewClient() {
            @SuppressLint("RestrictedApi")
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                println("onPageFinished   $url")
                webview_layout.visibility = View.VISIBLE
                fab_download.visibility = View.VISIBLE
                progressInvoice.visibility = View.GONE
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                println("onPageStarted $url")
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    println("""shouldOverrideUrlLoading ${request!!.url}""")
                }
                return false
            }
        }
        webview_layout.loadUrl(webUrl)

        webview_layout.webChromeClient = object : WebChromeClient() {

            override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsAlert $message")
                return true
            }

            override fun onJsBeforeUnload(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsBeforeUnload $message")
                return true
            }

            override fun onConsoleMessage(message: String?, lineNumber: Int, sourceID: String?) {
                println("onConsoleMessage $message")
                super.onConsoleMessage(message, lineNumber, sourceID)
            }

            override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                println("onJsConfirm $message")
                return true
            }

            override fun onJsPrompt(
                view: WebView?,
                url: String?,
                message: String?,
                defaultValue: String?,
                result: JsPromptResult?
            ): Boolean {
                println("onJsPrompt $message")
                return true
            }

            @SuppressLint("SetJavaScriptEnabled")
            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ): Boolean {
                val newWebView = WebView(this@InvoiceViewActivity)
                newWebView.settings.javaScriptEnabled = true
                newWebView.settings.setSupportZoom(true)
                newWebView.settings.builtInZoomControls = true
                newWebView.settings.pluginState = WebSettings.PluginState.ON
                newWebView.settings.setSupportMultipleWindows(true)
                view!!.addView(newWebView);
                val transport = resultMsg!!.obj as WebView.WebViewTransport
                transport.webView = newWebView
                resultMsg.sendToTarget()

                newWebView.webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view!!.loadUrl(request!!.url.toString())
                        }
                        return true
                    }
                }
                return true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_download -> {
                createWebPrintJob(webview_layout)
            }
            android.R.id.home -> {
                onBackPressed()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createWebPrintJob(webView: WebView) {
        val printManager = getSystemService(Context.PRINT_SERVICE) as PrintManager
        val printAdapter = webView.createPrintDocumentAdapter()
        val jobName = "invoice_" + invoice_id
        printManager.print(jobName, printAdapter, PrintAttributes.Builder().build())
    }
}
