package com.xonware.xims.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.adapter.CreateTicketAdapter
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.UpdateProfileResponse
import com.xonware.xims.response.add_ticket.AddTicketResponse
import kotlinx.android.synthetic.main.activity_create_ticket.*
import org.json.JSONObject

class CreateTicketActivity : BaseActivity(), WebServices.SetResponse{


    var addTicketResponse: AddTicketResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_ticket)

        supportActionBar!!.title = "Add New Ticket"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        webServices = WebServices(this@CreateTicketActivity)
        preferences = Preferences.getInstance(this@CreateTicketActivity)

        tvUserId.text = preferences!!.getPreferencesString(getString(R.string.pref_customer_name))
      //  tvZone.text = preferences!!.getPreferencesString(getString(R.string.pref_user_type))

        webServices!!.callWebGetService(getString(R.string.api_ticket) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)))
        webServices!!.mResponseInterface = this

        btnSubmitTicket.setOnClickListener {
            println("is selected response " + addTicketResponse!!.selectedOption)
            val descriptionStr = etDescription.text.toString().trim()
            if (descriptionStr.isNullOrEmpty()) {
                showToast("Please Enter Description")
            } else if (addTicketResponse!!.selectedOption == 0) {
                showToast("Please Select Any Option")
            } else {
                addTicketResponse!!.user = preferences!!.getPreferencesString(getString(R.string.pref_customer_name))
                addTicketResponse!!.ticketDesc = descriptionStr
                addTicketResponse!!.userChk = true
                val jsonString = Gson().toJson(addTicketResponse)
                webServices!!.callWebJsonObjectPostService(getString(R.string.api_ticket), JSONObject(jsonString))
                webServices!!.mResponseInterface = this
            }
        }
    }

    override fun onSuccess(methodName: String, response: String?) {
        println("response   " + response)
        when (methodName) {
            getString(R.string.api_ticket) -> {
               var ticketCreated = Gson().fromJson(response, UpdateProfileResponse::class.java)
                if (ticketCreated.status==true){
                    showToast(ticketCreated.message!!)
                    onBackPressed()
                }else{
                    showToast("Unable to add ticket")
                }
            }
            else -> {
                addTicketResponse = Gson().fromJson(response, AddTicketResponse::class.java)
                if (addTicketResponse!!.oList != null) {
                    val mAdapter = CreateTicketAdapter(this, addTicketResponse)
                    val mLayoutManager = LinearLayoutManager(this)
                    rvTicketTitle.layoutManager = mLayoutManager
                    val dividerItemDecoration = DividerItemDecoration(
                        rvTicketTitle.context,
                        mLayoutManager.orientation
                    )
                    rvTicketTitle.addItemDecoration(dividerItemDecoration)
                    rvTicketTitle.itemAnimator = DefaultItemAnimator()
                    rvTicketTitle.adapter = mAdapter
                }
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
