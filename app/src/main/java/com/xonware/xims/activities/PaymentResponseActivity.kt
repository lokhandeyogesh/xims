package com.xonware.xims.activities

import android.content.Intent
import android.os.Bundle
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import kotlinx.android.synthetic.main.activity_payment_response.*

class PaymentResponseActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_response)
        supportActionBar!!.hide()

        if (intent != null && intent.getBooleanExtra(getString(R.string.is_payment_success), false)) {
            rlBackground.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
            payStatusIv.setImageDrawable(resources.getDrawable(R.drawable.payment_success))
            payStatusTextView.text = "Your Payment Was Successful"
            amountPayStatus.text = "Amount : \u20B9 ${intent.getStringExtra(getString(R.string.payment_amount))}"
            txnNumberPayStatus.text = "Transaction id : ${intent.getStringExtra(getString(R.string.payment_txn_id))}"
        } else {
            rlBackground.setBackgroundColor(resources.getColor(R.color.red))
            payStatusIv.setImageDrawable(resources.getDrawable(R.drawable.payment_failed))
            payStatusTextView.text = "Your Payment Was Failed"
            amountPayStatus.text = "Amount : \u20B9 ${intent.getStringExtra(getString(R.string.payment_amount))}"
            txnNumberPayStatus.text = "Transaction id : ${intent.getStringExtra(getString(R.string.payment_txn_id))}"
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@PaymentResponseActivity, HomeActivity::class.java))
        finish()
    }
}
