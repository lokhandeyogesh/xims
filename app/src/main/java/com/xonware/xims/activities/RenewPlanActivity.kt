package com.xonware.xims.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.TextViewCompat
import com.androidnetworking.error.ANError
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.xonware.xims.NothingSelectedSpinnerAdapter
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.CouponCodeResponse
import com.xonware.xims.response.payment_details.PaymentDetailsResponse
import com.xonware.xims.response.plan_list.PDlst
import com.xonware.xims.response.plan_list.Plan
import com.xonware.xims.response.plan_list.PlanListResponse
import com.xonware.xims.response.plan_list.PlanNew
import com.xonware.xims.response.tax_response.TaxDetailsResponse
import kotlinx.android.synthetic.main.activity_renew_plan.*
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import org.json.JSONObject
import java.io.Serializable
import java.text.DecimalFormat


class RenewPlanActivity : BaseActivity(), WebServices.SetResponse, AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var arrFinal = ArrayList<PlanNew>()
    var responsePlan = PlanListResponse()
    var taxresponse = TaxDetailsResponse()
    var paymentDetails = PaymentDetailsResponse()
    var selectedPlanName = ArrayList<Plan>()
    var selectedPlanId = String()
    var couponCodeStr: String? = null
    var selectedBillingArr = ArrayList<PDlst>()
    var selectedBillingItem = PDlst()
    var cgstTax: Float = 0.0f
    var sgstTax: Float = 0.0f

    var sheetBehavior: BottomSheetBehavior<*>? = null
    var layoutBottomSheet: RelativeLayout? = null
    lateinit var paymentType : String

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_renew_plan)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        if (intent.getStringExtra("data").contains("Instant")){
            tvRenewText.visibility = View.VISIBLE
            supportActionBar!!.title = "Instant Renew"
            paymentType = "Renew"
        }else{
            supportActionBar!!.title = "Advance Renew"
            paymentType = "Adv Renew"
        }

//        val snackbar = Snackbar
//            .make(coordinatorLayout, "Your Current Plan Will be Discarded If You Renew.", Snackbar.LENGTH_LONG)
//        snackbar.show()
    }

    override fun onResume() {
        init()
        super.onResume()
    }

    private fun init() {
//        println("init size "+taxresponse.taxlst!!.size)o
        layoutBottomSheet = findViewById(R.id.bottom_sheet)

        cbTerms.isChecked = false
        cbTerms.isEnabled = false
        table_payment.visibility = View.GONE

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet)
        sheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
        taxresponse = TaxDetailsResponse()
        responsePlan = PlanListResponse()
        paymentDetails = PaymentDetailsResponse()

        webServices = WebServices(this@RenewPlanActivity)
        preferences = Preferences.getInstance(this@RenewPlanActivity)
        val jsonObject: MutableMap<String, String> = mutableMapOf()
        webServices!!.callWebPostService(
            getString(R.string.api_plan) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)),
            jsonObject
        )
        webServices!!.mResponseInterface = this

        webServices!!.callWebGetService(
            getString(R.string.api_tax_details)

        )
        webServices!!.mResponseInterface = this

        spPlanType.onItemSelectedListener = this
        spPlan.onItemSelectedListener = this
        spBillingCycle.onItemSelectedListener = this

        tvTerms.setOnClickListener(this)
        btnInstantRenew.setOnClickListener(this)
        btnApplyCoupon.setOnClickListener(this)

        sheetBehavior!!.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {
            }

            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(p0: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })

        cbTerms.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                println("is checked " + sheetBehavior!!.state)
                sheetBehavior!!.state = (BottomSheetBehavior.STATE_EXPANDED)
                if (couponCodeStr != null) {

                } else {
                    /* tvPlanNameBtm.text = ":" + paymentDetails.planName
                     tvPlanTypeBtm.text = ":" + paymentDetails.planCategoryName
                     tvPlanValidityBtm.text = ":" + paymentDetails.billingCycleName
                     val onlyDate = ":" + paymentDetails.expDate!!.split("T")[0]
                     tvPlanExpiryBtm.text = onlyDate
                     tvPlanAmountBtm.text = ":\u20B9" + paymentDetails.planAmount.toString()
                     tvTaxAmountBtm.text = ":\u20B9" + paymentDetails.totalTaxAmount.toString()*/
                    tvTotalAmountBtm.text = "\u20B9" + paymentDetails.totalAmount.toString()
                    //tvTotalAmountBtm.text = "\u20B9" + tvTotalAmount.text.toString()
                    /* if (paymentDetails.planDiscountAmount != null) {
                         tvDiscTTl.visibility = View.VISIBLE
                         tvDiscountBtm.visibility = View.VISIBLE
                         tvDiscountBtm.text = paymentDetails.planDiscountAmount.toString()
                     } else {
                         tvDiscTTl.visibility = View.INVISIBLE
                         tvDiscountBtm.visibility = View.INVISIBLE
                     }*/
                }
            } else {
                println("is checked false" + sheetBehavior!!.state)
                sheetBehavior!!.state = (BottomSheetBehavior.STATE_HIDDEN)
            }
        }
    }

    override fun onSuccess(methodName: String, response: String?) {
        when (methodName) {
            getString(R.string.api_plan) + preferences!!.getPreferencesString(getString(R.string.pref_user_id)) -> {
                responsePlan = Gson().fromJson(response, PlanListResponse::class.java)
                if (!responsePlan.planNew.isNullOrEmpty()) {
                    setPlanType()
                    spPlan.isEnabled = false
                    spBillingCycle.isEnabled = false
                }
            }
            getString(R.string.api_tax_details) -> {
                taxresponse = Gson().fromJson(response, TaxDetailsResponse::class.java)
                println("tax response size " + taxresponse)
                println("init size " + taxresponse.taxlst!!.size)
                println("tax response size " + response)
                /*if (taxresponse.taxlst!!.size > 1) {
                    for (i in 0 until taxresponse.taxlst!!.size) {
                        if (taxresponse.taxlst!![i].taxName.equals("sgst", true)) {
                            sgstTax = taxresponse.taxlst!![i].taxPercent!!.toFloat()
                        } else if (taxresponse.taxlst!![i].taxName.equals("cgst", true)) {
                            cgstTax = taxresponse.taxlst!![i].taxPercent!!.toFloat()
                        }
                    }
                }*/

            }
            getString(R.string.api_payment_details) -> {
                paymentDetails = Gson().fromJson(response, PaymentDetailsResponse::class.java)
                if (selectedBillingItem.amount != null) {
                    println("init size " + taxresponse.taxlst!!.size)
                    table_payment.visibility = View.VISIBLE
                    val amount = java.lang.Double.parseDouble(selectedBillingItem.amount.toString())
                    // val sGst = (amount / 100.0f) * sgstTax
                    //val cGst = (amount / 100.0f) * cgstTax
                    // val total = amount + sGst + cGst
                    var total = 0f
                    tvPlanAmount.text = selectedBillingItem!!.amount.toString()
                    //tvCgst.text = cGst.toString()
                    //tvSgst.text = sGst.toString()

                    cbTerms.isEnabled = true

                    for (i in 0 until taxresponse.taxlst!!.size) {
                        println("amount is " + amount)
                        println("tax is " + taxresponse.taxlst!![i].taxPercent!!.toFloat())
                        taxresponse.taxlst!![i].taxAnount =
                            (amount / 100f) * DecimalFormat("##.##").format(taxresponse.taxlst!![i].taxPercent!!.toFloat()).toFloat()
                        println("total is " + taxresponse.taxlst!![i].taxAnount)
                        total += taxresponse.taxlst!![i].taxAnount!!.toFloat()
                    }
                    println("total is $total")
                    paymentDetails.taxDetailsList = null
                    paymentDetails.taxDetailsList = taxresponse.taxlst!!
                    tvTotalAmount.text = (selectedBillingItem!!.amount!!.plus(total)).toString()

                    println("init size " + taxresponse.taxlst!!.size)
                    println("init size " + paymentDetails.taxDetailsList!!.size)
                    tableTaxes.removeAllViews()
                    for (j in 0 until taxresponse.taxlst!!.size) {
                        var tbrow = TableRow(this)
                        tbrow.gravity = Gravity.CENTER_HORIZONTAL
                        // tbrow. = TableRow.LayoutParams.MATCH_PARENT
                        tableTaxes.isStretchAllColumns = true
                        val t1v = TextView(this)
                        t1v.text = paymentDetails!!.taxDetailsList!![j].taxName
                        t1v.setTextColor(Color.BLACK)
                        val params = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
                        params.topMargin = 10
                        t1v.layoutParams = params
                        TextViewCompat.setTextAppearance(t1v, R.style.text_view_info)
                        t1v.setTypeface(null, Typeface.BOLD)
                        t1v.gravity = Gravity.CENTER_VERTICAL
                        tbrow.addView(t1v)
                        val t2v = TextView(this)
                        t2v.text =  DecimalFormat("###.##").format(paymentDetails!!.taxDetailsList!![j].taxAnount)
                        t2v.setTextColor(Color.BLACK)
                        t2v.layoutParams = params
                        t2v.layoutParams = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT)
                        TextViewCompat.setTextAppearance(t2v, R.style.text_view_info)
                        tbrow.addView(t2v)
                        tableTaxes.addView(tbrow)
                    }


                } else {
                    cbTerms.isEnabled = false
                    table_payment.visibility = View.GONE
                }
            }
            getString(R.string.api_coupon_check) -> {
                println("response " + response)
                if (!response.isNullOrEmpty()) {
                    val couponResponse = Gson().fromJson(response, CouponCodeResponse::class.java)
                    if (!couponResponse.couponType.isNullOrEmpty() && !couponResponse.couponValue.isNullOrEmpty()) {
                        getPaymentDetails()
                    } else {
                        showToast("Please Enter Valid Coupon")
                    }
                }
            }
            /* "https://test.instamojo.com/oauth2/token/" -> {
                 println("response  ")
                 preferences!!.setPreferencesBody(
                     getString(R.string.pref_instmojo_access_token),
                     (JSONObject(response!!)).getString("access_token")
                 )
                 val jsonObject = JSONObject()
                 jsonObject.put("purpose", "hgdhsgd")
                 jsonObject.put("amount", 20)
                 jsonObject.put("buyer_name", "Yolo")
                 jsonObject.put("email", "y@gmail.com")
                 jsonObject.put("phone", 9561424969)

                 webServices!!.callWebGetFormEncodeService(
                     "https://test.instamojo.com/v2/payment_requests/",
                     jsonObject,
                     (JSONObject(response!!)).getString("access_token")
                 )
                 webServices!!.mResponseInterface = this
             }
             "https://test.instamojo.com/v2/payment_requests/" -> {
                 val webUrl = (JSONObject(response!!)).getString("longurl")
                 preferences!!.setPreferencesBody(
                     getString(R.string.pref_payment_id_insta),
                     (JSONObject(response!!)).getString("id")
                 )
                 val jsonObject = JSONObject()
                 jsonObject.put("id", (JSONObject(response!!)).getString("id"))
                 jsonObject.put("name", "Yolo")
                 webServices!!.callWebGetFormEncodeService(
                     "https://test.instamojo.com/v2/gateway/orders/payment-request/",
                     jsonObject,
                     preferences!!.getPreferencesString(getString(R.string.pref_instmojo_access_token))
                 )
                 webServices!!.mResponseInterface = this
                 startActivityForResult(
                     Intent(this@RenewPlanActivity, WebViewActivity::class.java).putExtra(
                         "url_is",
                         webUrl + "?embed=form"
                     ), 101
                 )
             }
             "https://test.instamojo.com/v2/gateway/orders/payment-request/" -> {
                 println("order id response is " + response)
                 val orderId = (JSONObject(response!!)).getString("order_id")
                 println("order id response is " + orderId)
             }*/
        }
    }

    private fun setBillingCycle(planName: String) {
        val fiterList = selectedPlanName.single { it.planName == planName }
        selectedPlanId = fiterList.id!!.toString()
        // var selectedBillingArr1 = fiterList.pDlst as ArrayList<PDlst>
        selectedBillingArr = fiterList.pDlst as ArrayList<PDlst>
//        for (i in 0 until selectedBillingArr1.size) {
//            if (selectedBillingArr1[i].amount != 0f) {
//                selectedBillingArr.add(selectedBillingArr1[i])
//            }
//        }
        println("ArrayList<PlanNew> selectedBillingArr   " + selectedBillingArr.size)
        println("ArrayList<PlanNew> selectedBillingArr   " + selectedBillingArr)
        if (!selectedBillingArr.isNullOrEmpty()) {
            val adapter = CustomAdapter(
                this,
                android.R.layout.simple_spinner_item, selectedBillingArr
            )
            adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
            spBillingCycle.adapter = NothingSelectedSpinnerAdapter(
                adapter,
                R.layout.row_nothing_selected,
                this
            )
            spBillingCycle.isEnabled = true
        }
    }

    private fun setPlan(planCat: String) {
        val fiterList = arrFinal.single { it.planCat == planCat }
        selectedPlanName = (fiterList.plan as ArrayList<Plan>?)!!
        if (!selectedPlanName.isNullOrEmpty()) {
            val adapter = CustomAdapter(
                this,
                android.R.layout.simple_spinner_item, selectedPlanName
            )
            adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
            spPlan.adapter = NothingSelectedSpinnerAdapter(
                adapter,
                R.layout.row_nothing_selected,
                this
            )
            spPlan.isEnabled = true
            spPlanType.isEnabled = true
            spBillingCycle.isEnabled = false
            sheetBehavior!!.state = (BottomSheetBehavior.STATE_HIDDEN)
            cbTerms.isChecked = false
            cbTerms.isEnabled = false
        }
    }

    private fun setPlanType() {
        var arrList = responsePlan.planNew as ArrayList<PlanNew>
        arrFinal = ArrayList<PlanNew>()
        for (plan in arrList) {
            if (!plan.plan.isNullOrEmpty()) {
                arrFinal.add(plan)
            }
        }

        for (i in 0 until arrFinal.size) {
            for (j in 0 until arrFinal[i].plan!!.size) {
                val arrTemp1 = ArrayList<PDlst>()
                for (k in 0 until arrFinal!![i].plan!![j].pDlst!!.size) {
                    val arrTemp = arrFinal!![i].plan!![j].pDlst as ArrayList<PDlst>
                    Log.e("arrte${i}is ${j} is ${k}", arrTemp!!.size.toString())
                    if (arrTemp[k].amount != 0f) {
                        arrTemp1.add(arrTemp[k])
                    }
                }
                arrFinal!![i]!!.plan!![j].pDlst = arrTemp1

            }
        }


        val adapter = CustomAdapter(
            this,
            android.R.layout.simple_spinner_item, arrFinal
        )
        adapter.setDropDownViewResource(R.layout.spinner_item_dropdown)
        spPlanType.adapter = NothingSelectedSpinnerAdapter(
            adapter,
            R.layout.row_nothing_selected,
            this
        )
        spBillingCycle.isEnabled = false
        sheetBehavior!!.state = (BottomSheetBehavior.STATE_HIDDEN)
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spPlanType -> {
                println("plantype" + parent.getItemAtPosition(position))
                if (parent.getItemAtPosition(position) != null) {
                    setPlan(parent.getItemAtPosition(position).toString())
                }
            }
            R.id.spPlan -> {
                println("plantype" + parent.getItemAtPosition(position))
                if (parent.getItemAtPosition(position) != null) {
                    setBillingCycle(parent.getItemAtPosition(position).toString())
                }
            }
            R.id.spBillingCycle -> {
                println("spBillingCycle is   " + selectedBillingArr.size)
                if (parent.getItemAtPosition(position) != null) {
                    selectedBillingItem = selectedBillingArr[position - 1]
                    println("selectedPlanId $selectedPlanId")
                    println("selectedBillingItem ${selectedBillingItem.pid}")
                    getPaymentDetails()
                }
            }
        }
    }

    private fun getPaymentDetails() {
        val jsonObject = JSONObject()
        jsonObject.put("Cid", preferences!!.getPreferencesString(getString(R.string.pref_user_id)))
        jsonObject.put("planid", selectedPlanId)
        jsonObject.put("billingCycle", selectedBillingItem.BillingId)
        jsonObject.put("couponcode", couponCodeStr)
        jsonObject.put("paymentype", paymentType)
        webServices!!.callWebJsonObjectPostService(
            getString(R.string.api_payment_details),
            jsonObject
        )
        webServices!!.mResponseInterface = this
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvTerms -> {
                showTermsDialog()
                /* val jsonObject = JSONObject()
                 jsonObject.put("id", preferences!!.getPreferencesString(getString(R.string.pref_payment_id_insta)))
                 webServices!!.callWebGetServiceInstamojo(
                     "https://api.instamojo.com/v2/payments/${preferences!!.getPreferencesString(getString(R.string.pref_payment_id_insta))}/",
                     preferences!!.getPreferencesString(getString(R.string.pref_instmojo_access_token)).toString()
                 )
                 webServices!!.mResponseInterface = this*/
            }
            R.id.btnInstantRenew -> {
                /* val jsonObject: MutableMap<String, String> = mutableMapOf()
                 jsonObject["client_id"] = "qUZWCB0C9QhT1iinUemtv2ghusW5rVp8sKOHgVrn"
                 jsonObject.put(
                     "client_secret",
                     "JHAjWhYzHQ3ClTG8pZHgt2ql2tpdcItjkTDngv7XRPVogFESqaRoQaM2pN2GXhxPRz0WKWmAauDNthYFSuQmfRou1lycUwJvtUpf1c561SAFF4RdG5fPF10XTPWt52t6"
                 )
                 jsonObject.put("grant_type", "client_credentials")*/


                /* val jsonObject: MutableMap<String, String> = mutableMapOf()
                 jsonObject["client_id"] = "test_d0MEsHANQLTQqaCOB4b5kRIgG00K2v96VNO"
                 jsonObject.put(
                     "client_secret",
                     "test_MX8BJ4MnB2XaQtyMcgcvrky3345FelfDgRI1Yuv2ZrAfc3cjNE0c7BJCJMaPCwChzPECMa1nhiml418l5yl10EFrjlj4OCshGrlQSfjzdkEtBMVEIHNemLbNbrC"
                 )
                 jsonObject.put("grant_type", "client_credentials")
                 webServices!!.callWebPostService("https://test.instamojo.com/oauth2/token/", jsonObject)
                 webServices!!.mResponseInterface = this*/

                startActivity(
                    Intent(
                        this@RenewPlanActivity,
                        ChoosePayGateActivity::class.java
                    ).putExtra(getString(R.string.intent_payment_details), paymentDetails as Serializable)
                )
            }
            R.id.btnApplyCoupon -> {
                couponCodeStr = etCouponCode.text.toString().trim()
                if (!couponCodeStr.isNullOrEmpty()) {
                    val jsonObject = JSONObject()
                    jsonObject.put("CouponCode", couponCodeStr)
                    jsonObject.put("UserId", preferences!!.getPreferencesString(getString(R.string.pref_user_id)))
                    webServices!!.callWebJsonObjectPostService(getString(R.string.api_coupon_check), jsonObject)
                    webServices!!.mResponseInterface = this
                } else {
                    showToast("Please Enter a Coupon code!!")
                }
            }
        }
    }

    private fun showTermsDialog() {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Terms & Conditions")

        val wv = WebView(this)
        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)

        wv.loadUrl(getString(com.xonware.xims.R.string.link_terms_conditions))
        wv.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBar.visibility = View.VISIBLE
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                progressBar.visibility = View.VISIBLE
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = View.GONE
            }
        }
        alert.setView(wv)
        alert.setNegativeButton("Close", { dialog, id -> dialog.dismiss() })
        alert.show()
    }


    override fun onFailure(methodName: String, error: ANError) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 101) {
                println("data " + data!!.getStringExtra(getString(R.string.insta_pay_id)))
                println("data " + data!!.getStringExtra(getString(R.string.insta_pay_status)))
                val status = data!!.getStringExtra(getString(R.string.insta_pay_status))
                if (status.equals("success", true)) {
                    showToast("Success")
                } else {
                    showToast("Failed")
                }

            }
        }
    }

    internal inner class CustomAdapter<Any>(
        context: Context, textViewResourceId: Int,
        objects: ArrayList<Any>
    ) : ArrayAdapter<Any>(context, textViewResourceId, objects) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = super.getView(position, convertView, parent)
            if (view is TextView) {
                val face = Typeface.createFromAsset(assets, "fonts/myfont.ttf")
                view.typeface = face
                view.setPadding(10, 10, 10, 10)
            }
            return view
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
