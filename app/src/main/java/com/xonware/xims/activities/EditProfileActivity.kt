package com.xonware.xims.activities

import android.os.Bundle
import android.view.MenuItem
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.UpdateProfileResponse
import com.xonware.xims.response.userprofile.UserProfileResponse
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.json.JSONObject

class EditProfileActivity : BaseActivity(), WebServices.SetResponse {


    private var userId: String? = null
    private var id: String? = null
    lateinit var UserImagePat: String
    lateinit var NewPassword: String
    lateinit var CnfirmPasswo: String
    lateinit var OriginalPass: String
    lateinit var Base64String: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        supportActionBar!!.title = "Edit Profile"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        webServices = WebServices(this@EditProfileActivity)
        preferences = Preferences.getInstance(this@EditProfileActivity)

        getUserProfile()
        btnSaveChanges.setOnClickListener {
            val firstName = etFirstname.text.toString().trim()
            val lastName = etLastname.text.toString().trim()
            val mobile = etMobile.text.toString().trim()
            val email = etEmail.text.toString().trim()

            when {
                firstName.isNullOrEmpty() -> {
                    showToast("Please Enter First Name")
                }
                lastName.isNullOrEmpty() -> {
                    showToast("Please Enter Last Name")
                }
                mobile.isNullOrEmpty() -> {
                    showToast("Please Enter Mobile Number")
                }
                !isValidMobile(mobile) -> {
                    showToast("Please Enter Valid Mobile Number")
                }
                email.isNullOrEmpty() -> {
                    showToast("Please Enter Email")
                }
                !isValidMail(email) -> {
                    showToast("Please Enter Valid Email")
                }
                else -> {
                    val jsonObject = JSONObject()
                    jsonObject.put("Id", id.toString())
                    jsonObject.put("UserId", userId.toString())
                    jsonObject.put("FirstName", firstName)
                    jsonObject.put("LastName", lastName)
                    jsonObject.put("MobileNo", mobile)
                    jsonObject.put("Email", email)
                    /*jsonObject.put("UserImagePath",  UserImagePat)
                    jsonObject.put("NewPassword",  NewPassword)
                    jsonObject.put("CnfirmPassword",  CnfirmPasswo)
                    jsonObject.put("OriginalPassword",  OriginalPass)
                    jsonObject.put("Base64String",  Base64String)*/
                    webServices!!.callWebJsonObjectPostService(getString(R.string.api_user_profile), jsonObject)
                    webServices!!.mResponseInterface = this
                }
            }
        }

    }

    private fun getUserProfile() {
        webServices!!.callWebGetService(
            getString(R.string.api_user_profile) + "/" + preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        println("reponse profile" + response)

        when (methodName) {
            getString(R.string.api_user_profile) + "/" + preferences!!.getPreferencesString(getString(R.string.pref_user_id)) -> {
                val userProfile = Gson().fromJson(response, UserProfileResponse::class.java)
                if (userProfile != null) {
                    userId = userProfile.userId!!.toString()
                    id = userProfile.id!!.toString()
                    etFirstname.setText(userProfile!!.firstName!!)
                    etLastname.setText(userProfile!!.lastName!!)
                    etMobile.setText(userProfile!!.mobileNo!!)
                    etEmail.setText(userProfile!!.email!!)

                    /*  UserImagePat = userProfile.userImagePath.toString()
            NewPassword = userProfile.newPassword.toString()
            CnfirmPasswo = userProfile.cnfirmPassword.toString()
            OriginalPass = userProfile.originalPassword.toString()
            Base64String = userProfile.base64String.toString()*/
                }
            }
            getString(R.string.api_user_profile) -> {
                val userProfile = Gson().fromJson(response, UpdateProfileResponse::class.java)
                if (userProfile.status == true) {
                    showToast(userProfile.message!!)
                }else{
                    showToast(userProfile.message!!)
                }
                onBackPressed()
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
        println("error is " + error.errorCode)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
