package com.xonware.xims.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.fxn.OnBubbleClickListener
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.xonware.xims.R
import com.xonware.xims.fragment.*
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar!!.hide()

        preferences = Preferences.getInstance(this@HomeActivity)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.viewPager, HomeFragment())
        transaction.commit();

        navigation.addBubbleListener(object : OnBubbleClickListener {
            override fun onBubbleClick(id: Int) {
                when (id) {
                    R.id.action_home -> {
                        loadFragment(HomeFragment())
                    }
                    R.id.action_invoices -> loadFragment(InvoicesFragment())
                    R.id.action_history -> loadFragment(PayHistoryFragment())
                    R.id.action_tickets -> loadFragment(TicketsFragment())
                    R.id.action_profile -> loadFragment(ProfileFragment())
                    R.id.action_plans -> loadFragment(PlansListFragment())
                    //R.id.action_feedback -> startActivity(Intent(this@HomeActivity,FeedbackFragment::class.java))
                }
            }
        })

        println("pref diplsyinvoide cust type"+preferences!!.getPreferencesBoolean(
            getString(R.string.pref_display_invoice),
            false
        ))
//        navigation.removeView(navigation.findViewById(R.id.action_invoices))
//        navigation.removeView(navigation.findViewById(R.id.action_plans))

        if (preferences!!.getPreferencesBoolean(
                getString(R.string.pref_display_invoice),
                false
            ) && preferences!!.getPreferencesString(getString(R.string.pref_cust_type))!!.contains("sharing",true)
        ) {
//            navigation.addView(navigation.findViewById(R.id.action_invoices))
        }else{
            navigation.removeView(navigation.findViewById(R.id.action_invoices))
        }

        if (preferences!!.getPreferencesBoolean(getString(R.string.pref_display_plan), false) && preferences!!.getPreferencesString(getString(R.string.pref_cust_type))!!.contains("sharing",true)) {
//            navigation.addView(navigation.findViewById(R.id.action_plans))
        }else{
            navigation.removeView(navigation.findViewById(R.id.action_plans))
        }

        if (!preferences!!.getPreferencesBoolean(getString(R.string.pref_show_tickets),false)){
            navigation.removeView(navigation.findViewById(R.id.action_tickets))
        }

        //Disable ViewPager Swipe
        viewPager.setOnTouchListener { v, event ->
            return@setOnTouchListener false
        }

        //setupViewPager(viewPager)
    }

    private object BottomNavigationHelper {
        @SuppressLint("RestrictedApi")
        internal fun removeShiftMode(view: BottomNavigationView) {
            val menuView = view.getChildAt(0) as BottomNavigationMenuView
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView

                item.setShifting(false)
                item.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED)

                // set once again checked value, so view will be updated

                item.setChecked(item.itemData.isChecked)
            }
        }
    }


    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.viewPager, fragment)
        transaction.commit();
    }


    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(HomeFragment(), "Home")
        adapter.addFrag(InvoicesFragment(), "Invoices")
        adapter.addFrag(PayHistoryFragment(), "History")
        adapter.addFrag(TicketsFragment(), "Tickets")
        adapter.addFrag(ProfileFragment(), "Profile")
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}
