package com.xonware.xims.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.xonware.xims.R
import com.xonware.xims.adapter.DataUsageAdapter
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.datausages.DataList
import com.xonware.xims.response.datausages.DataUsageResponse
import kotlinx.android.synthetic.main.activity_data_usage.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DataUsageActivity : BaseActivity(), WebServices.SetResponse, View.OnClickListener {
    private val REQUEST_GET_DATE = 3
    var startDate = true
    var startDateStr: String? = null
    var endDateStr: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_usage)

        supportActionBar!!.title = "Data Usage"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        webServices = WebServices(this)
        preferences = Preferences.getInstance(this)

        startDateStr = preferences!!.getPreferencesString(getString(R.string.pref_start_date))!!
        endDateStr = preferences!!.getPreferencesString(getString(R.string.pref_end_date))!!

        getDataUsage()


        tvStartDateT.setOnClickListener(this)
        tvEndDateT.setOnClickListener(this)
        btnSearch.setOnClickListener(this)
    }

    private fun getDataUsage() {
        val jsonObject: MutableMap<String, String> = mutableMapOf()
        jsonObject["FromDate"] = startDateStr!!
        jsonObject["ToDate"] = endDateStr!!
         jsonObject["CustId"] = preferences!!.getPreferencesString(getString(R.string.pref_user_id))!!
        //jsonObject["CustId"] = "724"
        webServices!!.callWebPostService(getString(R.string.api_data_usage), jsonObject)
        webServices!!.mResponseInterface = this
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvStartDateT -> {
                startDate = true
                pickDate()
            }
            R.id.tvEndDateT -> {
                startDate = false
                pickDate()
            }
            R.id.btnSearch -> {
                startDate = false
                getDataUsage()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_GET_DATE -> if (resultCode == Activity.RESULT_OK) {
                val setDate = data!!.getBundleExtra("set_date")
                val setDay = setDate.getInt("day")
                val setMonth = setDate.getInt("month")
                val setYear = setDate.getInt("year")
                println("$setDay $setMonth $setYear")
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccess(methodName: String, response: String?) {
        val datausageResponse = Gson().fromJson(response, DataUsageResponse::class.java)
        tvStartDateT.text = datausageResponse.fromDate
        tvEndDateT.text = datausageResponse.toDate
        tvUploadT.text = datausageResponse.upload
        tvDownloadT.text = datausageResponse.download
        tvTotalDataT.text = datausageResponse.total

        println("response invoices $response")
        val mAdapter = DataUsageAdapter(this, datausageResponse.dataList as ArrayList<DataList>)
        val mLayoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(
            rvDataUsages.context,
            mLayoutManager.orientation
        )
        rvDataUsages.addItemDecoration(dividerItemDecoration)
        rvDataUsages.setLayoutManager(mLayoutManager)
        rvDataUsages.setItemAnimator(DefaultItemAnimator())
        rvDataUsages.setAdapter(mAdapter)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }

    fun pickDate() {
        DatePickerDialog(
            this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    var date: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "dd-MMM-yy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            returnDate = sdf.format(myCalendar.time)
            if (startDate) {
                startDateStr = sdf.format(myCalendar.time)
                tvStartDateT.text = startDateStr
            } else {
                endDateStr = sdf.format(myCalendar.time)
                tvEndDateT.text = endDateStr
            }
            println("return date is $returnDate")
        }
}
