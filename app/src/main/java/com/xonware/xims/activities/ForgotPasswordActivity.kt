package com.xonware.xims.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.androidnetworking.error.ANError
import com.xonware.xims.R
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONObject

class ForgotPasswordActivity : BaseActivity(), WebServices.SetResponse {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        supportActionBar!!.hide()
        webServices = WebServices(this@ForgotPasswordActivity)
        preferences = Preferences.getInstance(this@ForgotPasswordActivity)


        btnRequest.setOnClickListener {
            val userid = etUserIdFP.text.toString().trim()
            val email = etEmailFP.text.toString().trim()
            if (userid.isEmpty()){
                showToast("Please Enter UserId")
            }else if (email.isEmpty()){
                showToast("Please Enter Email-Id")
            }else if(!email.isEmailValid()){
                showToast("Please Enter Valid Email-Id")
            }else{
                val jsonOBject = mutableMapOf<String,String>()
                jsonOBject["UserId"] = userid
                jsonOBject["Emailid"] = email
                webServices!!.callWebPostService("AForgotPassword",jsonOBject)
                webServices!!.mResponseInterface = this
            }
        }
    }

    override fun onSuccess(methodName: String, response: String?) {
        println("forgot "+response)
    }

    override fun onFailure(methodName: String, error: ANError) {
    }
}
