package com.xonware.xims.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.instamojo.android.Instamojo
import com.xonware.xims.paytm.PaytmActivity
import com.xonware.xims.R
import com.xonware.xims.adapter.OnlineGatewayAdapter
import com.xonware.xims.global.BaseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.instamojo.InstamojoActivity
import com.xonware.xims.response.gatewaylist.GatewayListResponse
import com.xonware.xims.response.gatewaylist.Glst
import com.xonware.xims.response.payment_details.PaymentDetailsResponse
import kotlinx.android.synthetic.main.activity_choose_pay_gate.*


class ChoosePayGateActivity : BaseActivity(), WebServices.SetResponse {

    var paymentDetails = PaymentDetailsResponse()
    var orderId = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_pay_gate)

        setUpActionBar()
        init()
        getPayGatewayList()
    }

    override fun onResume() {
        super.onResume()
        println("on resume called ")
    }

    private fun getPayGatewayList() {
        webServices!!.callWebGetService(
            getString(R.string.api_get_getway_list) + preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        webServices!!.mResponseInterface = this
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        webServices = WebServices(this@ChoosePayGateActivity)
        preferences = Preferences.getInstance(this@ChoosePayGateActivity)

        paymentDetails =
            intent.getSerializableExtra(getString(R.string.intent_payment_details)) as PaymentDetailsResponse

        Instamojo.getInstance().initialize(this, Instamojo.Environment.PRODUCTION)

        tvPlanNameBtm.text = ":" + paymentDetails.planName
        tvPlanTypeBtm.text = ":" + paymentDetails.planCategoryName
        tvPlanValidityBtm.text = ":" + paymentDetails.billingCycleName
        val onlyDate = ":" + paymentDetails.expDate!!.split("T")[0]
        tvPlanExpiryBtm.text = onlyDate
        tvPlanAmountBtm.text = ":\u20B9" + paymentDetails.planAmount.toString()
        tvTaxAmountBtm.text = ":\u20B9" + paymentDetails.totalTaxAmount.toString()
        tvTotalAmountBtm1.text = "\u20B9" + paymentDetails.totalAmount.toString()
        //tvTotalAmountBtm.text = "\u20B9" + tvTotalAmount.text.toString()
        if (paymentDetails.planDiscountAmount != null) {
            tvDiscTTl.visibility = View.VISIBLE
            tvDiscountBtm.visibility = View.VISIBLE
            tvDiscountBtm.text = paymentDetails.planDiscountAmount.toString()
        } else {
            tvDiscTTl.visibility = View.INVISIBLE
            tvDiscountBtm.visibility = View.INVISIBLE
        }
    }

    private fun generateOrderId() {
        webServices!!.callWebGetService(
            getString(R.string.api_gen_order_id) + preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        webServices!!.mResponseInterface = this
    }

    private fun setUpActionBar() {
        supportActionBar!!.title = "Choose Payment Gateway"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    private fun paymentGatewayList(resposnseList: List<Glst>) {
        val mAdapter = OnlineGatewayAdapter(this, resposnseList)
        val mLayoutManager = LinearLayoutManager(this)
        rvListOfGateway.layoutManager = mLayoutManager
        val dividerItemDecoration = DividerItemDecoration(
            rvListOfGateway.context,
            mLayoutManager.orientation
        )
        rvListOfGateway.addItemDecoration(dividerItemDecoration)
        rvListOfGateway.itemAnimator = DefaultItemAnimator()
        rvListOfGateway.adapter = mAdapter

        mAdapter.setOnItemClickListener(object : OnlineGatewayAdapter.MyOnClickListener {
            override fun onRecyclerItemClick(movie: Glst) {
                println("payment gateway itme clicked "+movie.gatewayName)
                when {
                    movie.gatewayName!!.contains("instamojo", true) -> {
                        startActivity(
                            Intent(
                                this@ChoosePayGateActivity,
                                InstamojoActivity::class.java
                            ).putExtra(
                                getString(R.string.intent_payment_details),
                                paymentDetails
                            ).putExtra(
                                "data",
                                movie
                            )
                        )
                    }
                    movie.gatewayName!!.contains("paytm", true) -> {
                            startActivity(
                                Intent(this@ChoosePayGateActivity, PaytmActivity::class.java).putExtra(
                                    getString(R.string.intent_payment_details),
                                    intent.getSerializableExtra(getString(R.string.intent_payment_details))
                                ).putExtra(
                                    "data",
                                    movie
                                )
                            )
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccess(methodName: String, response: String?) {
        when {
            methodName.contains(getString(R.string.api_get_getway_list), true) -> {
                if (!response.isNullOrEmpty()) {
                    val resposnseList = Gson().fromJson(response, GatewayListResponse::class.java)
                    paymentGatewayList(resposnseList.glst)
                }
            }
            else -> {
                println("response id \n $methodName \n" + response)
            }
        }
    }

    override fun onFailure(methodName: String, error: ANError) {
        println("error is " + methodName + ">>" + error.message)
    }
}
