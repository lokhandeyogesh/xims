package com.xonware.xims.instamojo

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.instamojo.android.Instamojo
import com.xonware.xims.R
import com.xonware.xims.activities.PaymentResponseActivity
import com.xonware.xims.global.Preferences
import com.xonware.xims.global.WebServices
import com.xonware.xims.response.CommonMessageResoponse
import com.xonware.xims.response.dashboard.DashboardResponse
import com.xonware.xims.response.gatewaylist.Glst
import com.xonware.xims.response.payment_details.PaymentDetailsResponse
import kotlinx.android.synthetic.main.activity_instamojo.*
import org.json.JSONArray
import org.json.JSONObject

class InstamojoActivity : com.xonware.xims.global.BaseActivity(), WebServices.SetResponse,
    Instamojo.InstamojoPaymentCallback {

    private lateinit var kjsonoBject: JSONObject
    private var transactionID: String? = null
    private var paymentStatus: String = ""
    var paymentDetails = PaymentDetailsResponse()
    var instamojoPojo = Glst()
    var orderId = String()
    var getwayId = String()
    var getwayName = String()
    var transactionId = String()
    var transactionIdForServer = String()
    var client_id = String()
    var client_secret = String()
    lateinit var totalAmount: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instamojo)
        supportActionBar!!.hide()
        progressBar.visibility = View.VISIBLE
        init()
        generateOrderId()
    }

    private fun init() {
        webServices = WebServices(this@InstamojoActivity)
        preferences = Preferences.getInstance(this@InstamojoActivity)

        paymentDetails =
            intent.getSerializableExtra(getString(R.string.intent_payment_details)) as PaymentDetailsResponse
        instamojoPojo =
            intent.getSerializableExtra("data") as Glst

        client_id = instamojoPojo.gatewayUserId!!
        client_secret = instamojoPojo.gatewayKey!!
        getwayId = instamojoPojo.id.toString()!!
        getwayName = instamojoPojo.gatewayName.toString()!!
        totalAmount = paymentDetails!!.totalAmount!!.toString()

        println("payment details ${paymentDetails.taxDetailsList}")

        Instamojo.getInstance().initialize(this, Instamojo.Environment.PRODUCTION)
        //Instamojo.getInstance().initialize(this, Instamojo.Environment.PRODUCTION)
    }

    private fun generateOrderId() {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", transactionIdForServer)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", "Initiated")
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        jsonObject.put(
            "Id", preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        println("get generate order " + jsonObject)
        webServices!!.callWebGetService(
            getString(R.string.api_gen_order_id) + preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )/*, jsonObject*/
        )
        webServices!!.mResponseInterface = this
    }

    override fun onResume() {
        super.onResume()
        println("on resume is " + totalAmount)
    }

    //for instamojo
    fun callTokenForInstamojo() {
        //live credentials
        val jsonObject: MutableMap<String, String> = mutableMapOf()
        jsonObject["client_id"] = client_id
        jsonObject.put(
            "client_secret",
            client_secret
        )
        jsonObject.put("grant_type", "client_credentials")

        //test credentials
//        val jsonObject: MutableMap<String, String> = mutableMapOf()
//        jsonObject["client_id"] = "test_d0MEsHANQLTQqaCOB4b5kRIgG00K2v96VNO"
//        jsonObject.put(
//            "client_secret",
//            "test_MX8BJ4MnB2XaQtyMcgcvrky3345FelfDgRI1Yuv2ZrAfc3cjNE0c7BJCJMaPCwChzPECMa1nhiml418l5yl10EFrjlj4OCshGrlQSfjzdkEtBMVEIHNemLbNbrC"
//        )
//        jsonObject.put("grant_type", "client_credentials")
        webServices!!.callWebPostService(
            getString(R.string.api_get_access_token_instamojo),
            jsonObject
        )
        webServices!!.mResponseInterface = this
    }

    override fun onSuccess(methodName: String, response: String?) {
        when (methodName) {
            getString(R.string.api_gen_order_id_request_params) -> {
                println("transaction is " + transactionId)
                println("transaction is response" + response)
                createPaymentRequest()
            }

            getString(R.string.api_get_access_token_instamojo) -> {
                println("response  ")
                val dashboardResponse = Gson().fromJson(
                    preferences!!.getPreferencesString("dashboard_response"),
                    DashboardResponse::class.java
                )
                var mobile = "111111111"
                var email = "abc@xyz.com"
                if (!dashboardResponse.cust?.emailId.isNullOrEmpty()) {
                    email = dashboardResponse.cust?.emailId!!
                }
                if (!dashboardResponse.cust?.mobile.isNullOrEmpty()) {
                    mobile = dashboardResponse.cust?.mobile!!
                }

                preferences!!.setPreferencesBody(
                    getString(R.string.pref_instmojo_access_token),
                    (JSONObject(response!!)).getString("access_token")
                )
                val jsonObject = JSONObject()
                jsonObject.put("purpose", paymentDetails.billingCycleName)
                jsonObject.put("amount", paymentDetails.totalAmount)
                jsonObject.put("buyer_name", paymentDetails.userId)
                jsonObject.put("name", paymentDetails.userId)
                jsonObject.put("currency", "INR")
                jsonObject.put("transaction_id", orderId)
                jsonObject.put("email", email)
                jsonObject.put("phone", mobile)
                jsonObject.put(
                    "redirect_url",
                    "https://api.instamojo.com/integrations/android/redirect/"
                )
                jsonObject.put("description", paymentDetails.paymentType)
                jsonObject.put("env", Instamojo.Environment.PRODUCTION)
                //jsonObject.put("env", Instamojo.Environment.PRODUCTION)

                webServices!!.callWebPostServiceWithHeaders(
                    /*"https://test.instamojo.com/v2/payment_requests/"*/"https://api.instamojo.com/v2/gateway/orders/",
                    jsonObject,
                    (JSONObject(response)).getString("access_token")
                )
                webServices!!.mResponseInterface = this
            }
            /*"https://test.instamojo.com/v2/payment_requests/"*/
            "https://api.instamojo.com/v2/gateway/orders/" -> {
                preferences!!.setPreferencesBody(
                    getString(R.string.pref_payment_id_insta),
                    (JSONObject(response!!)).getJSONObject("order").getString("id")
                )
                //remove comment from following code for webview
                /* startActivityForResult(
                     Intent(this@ChoosePayGateActivity, WebViewActivity::class.java).putExtra(
                         "url_is",
                         webUrl + "?embed=form"
                     ).putExtra("order_id",orderId), INSTAMOJO_SUCCESS
                 )*/

                kjsonoBject = JSONObject()
                val json = (JSONObject(response)).getJSONObject("order")
                val ids = json.getString("id")
                kjsonoBject.put("id", ids)
                transactionIdForServer = ids

                println("id brfore set pay request "+kjsonoBject)

                setPaymentRequestData(ids)

                //createPaymentRequest()
            }
            getString(R.string.api_gen_order_id) + preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            ) -> {
                if (!response.isNullOrEmpty()) {
                    println("order is response " + Gson().toJson(response))
                    orderId = response!!.replace("\"", "")
                    println("order id is " + orderId)
                    callTokenForInstamojo()
                }
            }
            "https://api.instamojo.com/v2/gateway/orders/payment-request/" -> {
                println("response id request$response")
                println("response id amount is ${paymentDetails.totalAmount}")
                println("response id request" + (JSONObject(response!!)).getString("order_id"))
                transactionId = (JSONObject(response!!)).getString("order_id")
                Instamojo.getInstance()
                    .initiatePayment(this, transactionId, this)
//                setPaymentRequestData()
            }
            getString(R.string.api_mark_pay_status) -> {
                val responseObject = Gson().fromJson(response, CommonMessageResoponse::class.java)
                showToast(responseObject.msg.toString())
                if (paymentStatus.equals("Credit", true)) {
                    startActivity(
                        Intent(
                            this@InstamojoActivity,
                            PaymentResponseActivity::class.java
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_txn_id),
                            transactionID
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_amount),
                            totalAmount
                        )
                    )
                } else {
                    startActivity(
                        Intent(
                            this@InstamojoActivity,
                            PaymentResponseActivity::class.java
                        ).putExtra(getString(R.string.is_payment_success), false).putExtra(
                            getString(R.string.payment_txn_id),
                            transactionID
                        ).putExtra(getString(R.string.is_payment_success), true).putExtra(
                            getString(R.string.payment_amount),
                            totalAmount
                        )
                    )
                }
            }
            else -> {
                println("response id \n $methodName \n$response")
            }
        }
    }

    private fun createPaymentRequest() {
        webServices!!.callWebPostServiceWithHeaders(
            "https://api.instamojo.com/v2/gateway/orders/payment-request/",
            kjsonoBject,
            preferences!!.getPreferencesString(getString(R.string.pref_instmojo_access_token))!!
        )
        webServices!!.mResponseInterface = this
    }

    private fun setPaymentRequestData(ids: String) {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", ids)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", "Initiated")
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        jsonObject.put(
            "Id", preferences!!.getPreferencesString(
                getString(R.string.pref_user_id)
            )
        )
        println("api_gen_order_id_request_params " + jsonObject)
        webServices!!.callWebJsonObjectPostService(
            getString(R.string.api_gen_order_id_request_params), jsonObject
        )
        webServices!!.mResponseInterface = this

    }

    override fun onFailure(methodName: String, error: ANError) {
        println("error is " + methodName + ">>" + error.message)
        progressBar.visibility = View.GONE
        showToast("Something went wrong try again !!")
        onBackPressed()
    }

    //region Instamojo response
    override fun onPaymentCancelled() {
        println("payment cancelled instamojo")
        progressBar.visibility = View.GONE
        //showToast("Your Last Transaction Was Cancelled.")
        showAlertDialog(transactionIdForServer, orderId, "Your Last Transaction Was Cancelled.")
        // onBackPressed()
    }

    override fun onInstamojoPaymentComplete(
        orderID: String?,
        transactionID1: String?,
        paymentID: String?,
        paymentStatus1: String?
    ) {
        progressBar.visibility = View.GONE
        println("payment completed  instamojo orderID > $orderID")
        println("payment completed  instamojo transactionID > $transactionID1")
        println("payment completed  instamojo paymentID > $paymentID")
        println("payment completed  instamojo paymentStatus > $paymentStatus1")
        paymentStatus = paymentStatus1!!
        transactionID = transactionID1
        saveStatusOnServer(orderID, paymentStatus1)
    }

    private fun saveStatusOnServer(transactionID: String?, paymentStatus: String?) {
        val jsonObject = JSONObject()
        jsonObject.put("TransId", transactionIdForServer)
        jsonObject.put("PlanId", paymentDetails.planId)
        jsonObject.put("BillingCycleId", paymentDetails.billingCycleId)
        jsonObject.put("Amount", paymentDetails.totalAmount)
        jsonObject.put(
            "UserId",
            preferences!!.getPreferencesString(getString(R.string.pref_user_id))
        )
        jsonObject.put("GetwayName", getwayName)
        jsonObject.put("GetwayId", getwayId)
        jsonObject.put("TraStatus", paymentStatus)
        jsonObject.put("CouponCode", paymentDetails.couponCode.toString())
        jsonObject.put("PaymentType", paymentDetails.paymentType)

        val jsonArray = JSONArray()
        for (i in 0 until paymentDetails.taxDetailsList!!.size) {
            val jobject1 = JSONObject()
            jobject1.put("TaxId", paymentDetails.taxDetailsList!![i].taxName)
            jobject1.put("TaxAmt", paymentDetails.taxDetailsList!![i].taxAnount)
            jsonArray.put(jobject1)
        }

        jsonObject.put("Tlst", jsonArray)
        println("jsonoject is " + jsonObject)
        webServices!!.callWebJsonObjectPostService(
            getString(R.string.api_mark_pay_status),
            jsonObject
        )
        webServices!!.mResponseInterface = this
    }

    override fun onInitiatePaymentFailure(p0: String?) {
        println("payment onInitiatePaymentFailure instamojo$p0")
        progressBar.visibility = View.GONE
    }
    //endregion Instamojo response
}